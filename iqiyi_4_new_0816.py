import logging
import string
import json
import re
import time
import sys
import subprocess
import multiprocessing
try:
    from urllib.parse import urlparse, urlencode, quote, urljoin  # , parse_qs
except ImportError:
    from urlparse import urlparse, urlencode, quote, urljoin  # , parse_qs
import random
import requests
from bs4 import BeautifulSoup as BS
from bashlog import stdoutlogger
from base64 import b64encode
import math
import os

# IP去重，多连接，单并发,随机延迟

# uas  = ["Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",    "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",    "Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36", "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",    "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",    "Mozilla/5.0 (Linux; U; Android 4.0; en-us; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",    "Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"]

# uas = ["Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36", "Mozilla/5.0 (Linux; Android 4.4.4; en-us; Nexus 4 Build/JOP40D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 4.4.4; en-us; Nexus 5 Build/JOP40D) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 4.3; Nexus 7 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2307.2 Mobile Safari/537.36","Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; LGMS323 Build/KOT49I.MS32310c) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.1599.103 Mobile Safari/537.36","Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.0; en-us; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; Android 4.4.2; GT-I9505 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Version/1.5 Chrome/28.0.1500.94 Mobile Safari/537.36","Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.0; en-us; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36","Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 4.3; Nexus 7 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36","Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Mobile Safari/537.36","Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.1; en-us; GT-N7100 Build/JRO03C) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.0; en-us; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36","Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 4.3; Nexus 7 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36","Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36","Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.72 Safari/537.36","Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 4 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19","Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19","Mozilla/5.0 (Linux; Android 4.3; Nexus 7 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.72 Safari/537.36","Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; Nexus S Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1","Mozilla/5.0 (Linux; U; Android 2.3; en-us; SAMSUNG-SGH-I717 Build/GINGERBREAD) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1","Mozilla/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build/JSS15J) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.0; en-us; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 2.1; en-us; GT-I9000 Build/ECLAIR) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2","Mozilla/5.0 (Linux; U; Android 4.0; en-us; LT28at Build/6.1.C.1.111) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 2.3; en-us; SonyEricssonST25i Build/6.0.B.1.564) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1","Mozilla/5.0 (Linux; U; Android 4.2en- usSonyC6903 Build/14.1.G.1.518) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; U; Android 4.0.3; en-us; HTC Sensation Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30","Mozilla/5.0 (Linux; Android 4.0; LG-E975 Build/IMM76L) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"]

uas = [
    "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.3071.104 Safari/537.36",
    "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36",
    "Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 6 Build/LYZ28E) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.3071.104 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; U; Android 5.1; zh-cn; m1 metal Build/LMY47I) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/7.6 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; Android 5.1.1; vivo X7 Build/LMY47V; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/48.0.2564.116 Mobile Safari/537.36 baiduboxapp/8.6.5 (Baidu; P1 5.1.1)",
    "Mozilla/5.0 (Linux; U; Android 4.4.4; zh-cn; X9007 Build/KTU84P) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/7.6 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; U; Android 6.0.1; zh-cn; vivo Xplay6 Build/MXB48T) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/7.6 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; U; Android 6.0.1; zh-cn; Redmi 3X Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/7.2 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-CN; HUAWEI MT7-TL00 Build/HuaweiMT7-TL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.3.8.909 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; U; Android 6.0; zh-CN; MX6 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/40.0.2214.89 UCBrowser/11.5.6.946 Mobile Safari/537.36",
    "Mozilla/5.0 (Linux; U; Android 6.0; zh-cn; MX6 Build/MRA58K) AppleWebKit/537.36 (KHTML, like Gecko)Version/4.0 Chrome/37.0.0.0 MQQBrowser/7.6 Mobile Safari/537.36"
]
def get_targetrate(set_rate):
    temp = random.random()
    if temp <= set_rate:
        return True
    else:
        return False

def BeautifulSoup(content, markup_type='html5lib', **kwargs):
    return BS(content, markup_type, **kwargs)

# ua = random.choice(uas)


def headers(h=None):
    default = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'deflate',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Connection': 'keep-alive',
        # 'User-Agent': ua,
    }
    if h is not None:
        default.update(h)
    assert 'requests' not in default['User-Agent']
    return default

def base_headers(url, ua, body=None, ref=None):
    h = {'User-Agent': ua,}
    # p = urlparse(url)
    # h['Host'] = p.netloc
    # if body is not None:
    #     h['Content-Length'] = len(body)
    if ref is not None:
        h['Referer'] = ref

    return h

def encrypt(source, seed):
    # source = 'q=ct:1;ct:0&po=1&ul=http://m.iqiyi.com/w_19rrtl7yh5.html&b=4175004709&l=4175004709&d=112&v=0&pv=&pr=0&sv=4.0.10-ares3&gt=1&ea=1&veid=935afcba0ed5c14bf6e1dab8dfba111c&pt=0&ps=0'
    # seed = 'qc_100001_100186'
    # source_ords = list(map(ord, source))
    source_ords = map(ord, source)
    # assert source_ords == [113,61,99,116,58,49,59,99,116,58,48,38,112,111,61,49,38,117,108,61,104,116,116,112,58,47,47,109,46,105,113,105,121,105,46,99,111,109,47,119,95,49,57,114,114,116,108,55,121,104,53,46,104,116,109,108,38,98,61,52,49,55,53,48,48,52,55,48,57,38,108,61,52,49,55,53,48,48,52,55,48,57,38,100,61,49,49,50,38,118,61,48,38,112,118,61,38,112,114,61,48,38,115,118,61,52,46,48,46,49,48,45,97,114,101,115,51,38,103,116,61,49,38,101,97,61,49,38,118,101,105,100,61,57,51,53,97,102,99,98,97,48,101,100,53,99,49,52,98,102,54,101,49,100,97,98,56,100,102,98,97,49,49,49,99,38,112,116,61,48,38,112,115,61,48]

    result = []
    for index, each_source_ord in enumerate(source_ords):
        char_code = ord(seed[index % len(seed)])
        # res = xor(each_source_ord, char_code)
        res = each_source_ord ^ char_code
        result.append(res)

    # expected = [0,94,60,69,10,1,11,83,69,101,1,22,64,94,5,7,87,22,51,12,88,68,68,64,11,112,30,93,30,88,73,95,8,10,113,82,95,93,31,71,110,110,8,66,66,69,84,1,8,11,106,31,88,68,93,92,23,61,12,4,1,6,13,6,65,87,104,1,9,22,92,13,5,110,6,5,0,1,12,1,65,90,121,85,13,1,1,2,23,41,12,0,22,65,78,11,87,19,45,12,0,22,67,70,12,107,31,0,30,0,8,27,16,17,58,66,3,22,87,68,12,110,23,85,81,12,9,16,7,6,54,85,13,0,3,9,7,105,0,82,9,2,14,6,16,81,104,6,1,2,2,3,84,102,0,84,0,9,11,80,65,90,103,0,9,22,64,68,12,111,23,64,67,12,8]
    # for a, b in zip(expected, result):
    #     # print(a, b)
    #     if a != b:
    #         print(repr(chr(a)), '----', repr(chr(b)))
    b = bytearray(result)
    b64result = b64encode(b)
    return b64result


# res = encrypt("q=ct:1;ct:0&po=1&ul=http://m.iqiyi.com/w_19rrtl7yh5.html&b=4175004709&l=4175004709&d=112&v=0&pv=&pr=0&sv=4.0.10-ares3&gt=1&ea=1&veid=935afcba0ed5c14bf6e1dab8dfba111c&pt=0&ps=0", "qc_100001_100186")
# res = encrypt("q=ct:1;ct:0&po=1&ul=http://m.iqiyi.com/w_19rrtl7yh5.html&b=4175004709&l=4175004709&d=112&v=0&pv=&pr=0&sv=4.0.10-ares3&gt=1&ea=1&veid=935afcba0ed5c14bf6e1dab8dfba222c&pt=0&ps=0", "qc_100001_100186")
# print(res)
# print(quote(res.decode('utf-8')))
# exit()
logger = stdoutlogger(level=logging.DEBUG)

logging.getLogger('urllib3').setLevel(logging.ERROR)
# logger.debug('test')
def js_md5(source):
    def l(t):
        # for (var e = Array(), n = (1 << d) - 1, r = 0; r < t.length * d; r += d) e[r >> 5] |= (t.charCodeAt(r / d) & n) << r % 32;
                #   return e
        e = []
        d = 8
        n = (1 << 8) - 1
        r = 0
        while r < len(t) * d:
            # e.append(None)
            # print(r/d)
            # print('%s << %s = %s' % ((ord(t[r // d]) & n), (r % 32), (ord(t[r // d]) & n) << (r % 32)))
            # print(ord(t[r // d]))
            # print(ord(t[r // d]) & n)
            # print('%s & %s = %s' % (ord(t[r // d]), n, (ord(t[r // d]) & n)))
            right = (ord(t[r // d]) & n) << (r % 32)
            # print(e, r >> 5)
            # print('right=', right)
            if (r >> 5) == len(e):
                e.append(right)
            else:
                e[r >> 5] |= right
            r += d

        if None in e:
            raise RuntimeError('md5.l failed')
        return e

    def n(t, e):
                        # t[e >> 5] |= 128 << e % 32, t[14 + (e + 64 >>> 9 << 4)] = e;
                        # for (var n = 1732584193, r = -271733879, c = -1732584194, l = 271733878, f = 0; f < t.length; f += 16) {
                        #   var p = n,
                        #       h = r,
                        #       d = c,
                        #       g = l;
                        #   n = i(n, r, c, l, t[f + 0], 7, -680876936), l = i(l, n, r, c, t[f + 1], 12, -389564586), c = i(c, l, n, r, t[f + 2], 17, 606105819), r = i(r, c, l, n, t[f + 3], 22, -1044525330), n = i(n, r, c, l, t[f + 4], 7, -176418897), l = i(l, n, r, c, t[f + 5], 12, 1200080426), c = i(c, l, n, r, t[f + 6], 17, -1473231341), r = i(r, c, l, n, t[f + 7], 22, -45705983), n = i(n, r, c, l, t[f + 8], 7, 1770035416), l = i(l, n, r, c, t[f + 9], 12, -1958414417), c = i(c, l, n, r, t[f + 10], 17, -42063), r = i(r, c, l, n, t[f + 11], 22, -1990404162), n = i(n, r, c, l, t[f + 12], 7, 1804603682), l = i(l, n, r, c, t[f + 13], 12, -40341101), c = i(c, l, n, r, t[f + 14], 17, -1502002290), r = i(r, c, l, n, t[f + 15], 22, 1236535329), n = o(n, r, c, l, t[f + 1], 5, -165796510), l = o(l, n, r, c, t[f + 6], 9, -1069501632), c = o(c, l, n, r, t[f + 11], 14, 643717713), r = o(r, c, l, n, t[f + 0], 20, -373897302), n = o(n, r, c, l, t[f + 5], 5, -701558691), l = o(l, n, r, c, t[f + 10], 9, 38016083), c = o(c, l, n, r, t[f + 15], 14, -660478335), r = o(r, c, l, n, t[f + 4], 20, -405537848), n = o(n, r, c, l, t[f + 9], 5, 568446438), l = o(l, n, r, c, t[f + 14], 9, -1019803690), c = o(c, l, n, r, t[f + 3], 14, -187363961), r = o(r, c, l, n, t[f + 8], 20, 1163531501), n = o(n, r, c, l, t[f + 13], 5, -1444681467), l = o(l, n, r, c, t[f + 2], 9, -51403784), c = o(c, l, n, r, t[f + 7], 14, 1735328473), r = o(r, c, l, n, t[f + 12], 20, -1926607734), n = u(n, r, c, l, t[f + 5], 4, -378558), l = u(l, n, r, c, t[f + 8], 11, -2022574463), c = u(c, l, n, r, t[f + 11], 16, 1839030562), r = u(r, c, l, n, t[f + 14], 23, -35309556), n = u(n, r, c, l, t[f + 1], 4, -1530992060), l = u(l, n, r, c, t[f + 4], 11, 1272893353), c = u(c, l, n, r, t[f + 7], 16, -155497632), r = u(r, c, l, n, t[f + 10], 23, -1094730640), n = u(n, r, c, l, t[f + 13], 4, 681279174), l = u(l, n, r, c, t[f + 0], 11, -358537222), c = u(c, l, n, r, t[f + 3], 16, -722521979), r = u(r, c, l, n, t[f + 6], 23, 76029189), n = u(n, r, c, l, t[f + 9], 4, -640364487), l = u(l, n, r, c, t[f + 12], 11, -421815835), c = u(c, l, n, r, t[f + 15], 16, 530742520), r = u(r, c, l, n, t[f + 2], 23, -995338651), n = a(n, r, c, l, t[f + 0], 6, -198630844), l = a(l, n, r, c, t[f + 7], 10, 1126891415), c = a(c, l, n, r, t[f + 14], 15, -1416354905), r = a(r, c, l, n, t[f + 5], 21, -57434055), n = a(n, r, c, l, t[f + 12], 6, 1700485571), l = a(l, n, r, c, t[f + 3], 10, -1894986606), c = a(c, l, n, r, t[f + 10], 15, -1051523), r = a(r, c, l, n, t[f + 1], 21, -2054922799), n = a(n, r, c, l, t[f + 8], 6, 1873313359), l = a(l, n, r, c, t[f + 15], 10, -30611744), c = a(c, l, n, r, t[f + 6], 15, -1560198380), r = a(r, c, l, n, t[f + 13], 21, 1309151649), n = a(n, r, c, l, t[f + 4], 6, -145523070), l = a(l, n, r, c, t[f + 11], 10, -1120210379), c = a(c, l, n, r, t[f + 2], 15, 718787259), r = a(r, c, l, n, t[f + 9], 21, -343485551), n = s(n, p), r = s(r, h), c = s(c, d), l = s(l, g)
                        # }
                        # return Array(n, r, c, l)
        return None

    d = 8
    return n(l(t), len(t) * d)

def request_urls(task_parameters):
    # 错开并发
    time.sleep(random.randint(0, 5000) / 1000.0)
    # 传入到函数内部的参数格式
    # {
    # 'url':'http://m.iqiyi.com/w_19rtnwd5x5.html#vfrm=8-8-0-1',#'http://m.iqiyi.com/w_19rtnxsab9.html',
    # 'process_number':2,
    # 'videoid': 'someid',
    # 'video_info': {
    #                 'tvid': "8216139609",
    #                 'qipuId': "8216139609",
    #                 'vid': "6eed636cb58c5c822172f51f53a05f91",
    #                 'aid': "8216139609",
    #                 'duration': '405',
    #                 'shortTitle': "\u7EA6\u4F1A\u5C31\u662F\u8981\u4E0D\u4E00\u6837\u7684\u5986\u5BB9",
    #                 'cid': '13'
    #     },

    # 'old_cookie': [1, 2, 3, 4],

    # #视频相关的播放信息
    # 'click_ad_ratio':0.002,
    # 'ip': '121.69.50.22',
    # 'uv_ratio': 0.7,
    # }
    # iqiyi_video_url = 'http://m.iqiyi.com/w_19rtnxsab9.html'
    # var e=$.cookie.get(\"QC006\");return
    # e||(this.isNewUser=!0,e=$.crypto.md5(window.navigator.userAgent+document.cookie+Math.random()+1*(new
    # Date).getTime())
    logger.info('get task info')
    new_QC006 = ''.join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(32)
    )
    iqiyi_video_url = task_parameters['url']
    logger.info(iqiyi_video_url)
    config = {
        'url': iqiyi_video_url,
        # cookie: QC006
        # (this.isNewUser = !0, e = $.crypto.md5(window.navigator.userAgent + document.cookie + Math.random() + 1 * (new Date).getTime()), $.cookie.set("QC006", e
        'user_id': new_QC006,
        #  $.crypto.md5(user_id + "veid" + 1 * new Date))
        # videoEventId
        'video_event_id': ''.join(random.choice(string.digits + string.ascii_lowercase) for _ in range(32)),
        'a': '16bcdc24147b24a91aa713ea80f3da9c',  # adPlayerId, fixed
        # 'rid': 'e99c9cba12be5a427806f70ad0f0a342',
        'rid': ''.join(random.choice(string.digits + string.ascii_lowercase) for _ in range(32)),
        # cookie: QC112, =APP.$.crypto.md5(t+\"weid\"+1*new Date)
        "weid": ''.join(random.choice(string.digits + string.ascii_lowercase) for _ in range(32)),
        's': '1500423043298',
        # 'qxlc': '86110112',
        'platform_code': 'qc_100001_100186',
        'start_time': time.time() * 1000,
    }
    # print('打印时间')
    # print(config['start_time'])
    # print(int(config['start_time']))
    # Cookies定义
    # QC112:ce,weid,qtsid,在程序中使用config['weid']
    # iqiyi_user_id_QC112 = ''.join(
    #     random.choice(string.ascii_lowercase + string.digits) for _ in range(32)
    # )
    # new_QC007 = 'DIRECT'
    # config['weid']
    # iqiyi_user_id_QC008 = iqiyi_user_id_QC112
    # T00404通过show2的请求得到
    # __uuid=c3e3df80-fd02-ac6f-91ef-244dbf9e3a4f
    new_uuid = '%s-%s-%s-%s-%s' % (
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(8)),
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(4)),
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(4)),
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(4)),
        ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(12))
    )
    iqiyi_user_id_play_stream = '1'
    # config['start_time'] = time.time() * 1000
    timestap1 = str(int(config['start_time']) + 5000)[-3:]
    timestap2 = str(int(config['start_time']) + 5000)
    # new_dfp:前半部分字符串:dfp
    new_dfp = 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f@1503208305%s@%s' % (
        timestap1, timestap2)

    new_cookie = {
    '_iwt_id': '',#http://iqiyi.irs01.com/irt请求获得
    'QC006': new_QC006,#new_QC006,
    'QC112': config['weid'],
    'QC007': 'DIRECT',
    'QC008': config['weid'],
    'T00404': '',#cupid_cookie['T00404'],#http://t7z.cupid.iqiyi.com/show2获得,
    'uuid': new_uuid,#new_uuid,
    'dfp': new_dfp,#new_dfp,
    'play_stream': '1',
    'BAIDUID': '',#baiduid_cookie['BAIDUID'],#http://api.share.baidu.com/s.gif 获得,
    'HMACCOUNT': '',#humcout['HMACCOUNT'],#http://hm.baidu.com/hm.js?5df871ab99f94347b23ca224fc7d013f获得,
    'nu': '1',#新用户为1旧用户为0,是请求中的一个链接参数
    'Hm_lvt_5df871ab99f94347b23ca224fc7d013f': str(int((int(config['start_time']) + 5000) / 1000)),
    'Hm_lpvt_5df871ab99f94347b23ca224fc7d013f': str(int((int(config['start_time']) + 5000) / 1000))
    }
    config_playinfo = task_parameters['video_info']
    # config_playinfo = {
    # 'tvid': "8216139609",
    # 'qipuId': "8216139609",
    # 'vid': "6eed636cb58c5c822172f51f53a05f91",
    # 'aid': "8216139609",
    # 'duration': '405',
    # 'shortTitle': "\u7EA6\u4F1A\u5C31\u662F\u8981\u4E0D\u4E00\u6837\u7684\u5986\u5BB9",
    # 'cid': '13'
    # }

    # 使用新旧cookie的判断
    old_cookies = task_parameters['old_cookie']
    ip = task_parameters['ip']
    logger.info('ip: %s', ip)
    uv_ratio =  task_parameters['uv_ratio']
    logger.info('uv_ratio: %s', uv_ratio)
    click_ad_ratio = task_parameters['click_ad_ratio']
    logger.info('click_ad_ratio: %s', click_ad_ratio)
    video_name = task_parameters['videoid']
    if len(old_cookies) == 0:
        # 第一次运行没有cookie存档
        logger.info('first run')
        use_old_cookie = False
        nu = new_cookie['nu']
    else:
        # 有cookie存档，根据使用uv的比例来调整
        use_old_cookie_status = get_targetrate(uv_ratio)
        if use_old_cookie_status:
            logger.info('use old cookie')
            use_old_cookie = True
            old_cookie = random.choice(old_cookies)
            nu = old_cookie['nu']
        else:
            logger.info('set new user')
            use_old_cookie = False
            nu = new_cookie['nu']

    # url1
    # 请求开始
    url = iqiyi_video_url
    logger.info('GET  %s', url)
    # exit()
    ua = random.choice(uas)
    if use_old_cookie:
        cookie = {
            'QC006': old_cookie['QC006'],
            'T00404':old_cookie['T00404'],
            'Hm_lvt_5df871ab99f94347b23ca224fc7d013f': old_cookie['Hm_lvt_5df871ab99f94347b23ca224fc7d013f'],
            '__uuid': old_cookie['uuid'],
            'play_stream': old_cookie['play_stream'],
            '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
        content = resp.text
    else:
        resp = requests.get(url, headers=headers(base_headers(url, ua)), timeout=10)
    # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
        content = resp.text
    # search = re.compile(r"""Q.load\(['"](.*?)['"]\)""")
    # res = search.search(content)
    # print(res)
    # print(res.group(1))
    # app_js_url = urljoin('http://static.qiyi.com/js/html5/', res.group(1))
    # print(app_js_url)

    # TODO: extra config file


    #
    # url = app_js_url
    # logger.info('GET  %s', url)
    # resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)))
    # # soup = BeautifulSoup(resp.content)
    # cookie = resp.cookies
    # logger.info('RESP  %s - cookie:%s', url, dict(cookie))
    # exit()

    # http://iqiyi.irs01.com/irt?_iwt_t=i&_iwt_id=&_iwt_UA=UA-iqiyi-000008&r=0.69678626421605561500699015129
    # 'r': '0.%s' % (''.join(str(random.randint(0, 9)) for _ in range(31))),  # m.random() + +new Date()
    # 'r'：random() + time.time()
    # cookie 1
    strrandom = '0.%s' % (''.join(str(random.randint(0, 9))
                                for _ in range(16)))  # 16位
    strtime = str(int(config['start_time']) + random.randint(60, 105))  # 13位
    r = strrandom + strtime
    url = 'http://iqiyi.irs01.com/irt?%s' % urlencode({
        '_iwt_t': 'i',
        '_iwt_id': '',
        '_iwt_UA': 'UA-iqiyi-000008',
        'r': r,  # m.random() + +new Date()
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
        '_iwt_id': old_cookie['_iwt_id']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        irs01_cookie = resp.cookies
        cookie_isr01 = irs01_cookie['_iwt_id']
        logger.info('RESP  %s - cookie:%s', url, dict(irs01_cookie))
    else:
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    # soup = BeautifulSoup(resp.content)
        irs01_cookie = resp.cookies
        new_cookie['_iwt_id'] = dict(irs01_cookie)['_iwt_id']
        logger.info('RESP  %s - cookie:%s', url, dict(irs01_cookie))

    if re.search('pps.tv', config['url']):
        e = 2013
    else:
        e = 31




    # print(js_md5('ed8edf7b1c5753893ee5cf78a0779702weid1500592623483'))
    # exit()

    logger.info('baidu 1')
    # 20170728 add baidu
    # http://api.share.baidu.com/s.gif?l=http://m.iqiyi.com/w_19rtnxsab9.html
    # cookie 2
    url = 'http://api.share.baidu.com/s.gif?%s' % urlencode({
        'l': iqiyi_video_url
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
        'BAIDUID': old_cookie['BAIDUID']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie,timeout=10)
        baiduid_cookie = resp.cookies
        logger.info('RESP  %s - cookie:%s', url, dict(baiduid_cookie))
    else:
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
        baiduid_cookie = resp.cookies
        new_cookie['BAIDUID'] = dict(resp.cookies)['BAIDUID']
        logger.info(baiduid_cookie)
        logger.info('RESP  %s - cookie:%s', url, dict(baiduid_cookie))

    # http://msg.video.qiyi.com/jpb.gif?rdm=520225477&qtcurl=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&rfr=&flshuid=110acaed85e6283ba09e42a05181dbbf&lrfr=DIRECT&ppuid=&platform=31&weid=8725d9e4f055cd454c5c8e9bf47a764d&qtsid=8725d9e4f055cd454c5c8e9bf47a764d&msrc=&vfm=&re=768*1280&os=android&nu=0&as=6d547839f40c91e07c1a8c65880ae8a2&c1=13&tmplt=shortvideotplt
    # 每次打开nu的值在0和1之间转变
    # nu的值由 checkNewUser()来决定新旧用户
    # rdm:String(Math.floor(999999999*Math.random()))
    rdm = ''.join(['%s' % random.randint(0, 9) for num in range(0, 9)])
    url = 'http://msg.video.qiyi.com/jpb.gif?%s' % urlencode({
        "rdm": rdm,
        "qtcurl": iqiyi_video_url,
        "rfr": "",
        "flshuid": config['user_id'],
        "lrfr": "DIRECT",
        "ppuid": "",
        "platform": "31",
        "weid": config['weid'],
        "qtsid": config['weid'],
        "msrc": "",
        "vfm": "",
        "re": "1081.5*1921.5",  # screen size
        "os": "android",
        "nu": nu,
        # $.crypto.md5(e + user_id + t + "ChEnYH0415dadrrEDFf2016")
        "as": ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(32)),
        "c1": config_playinfo['cid'],
        "tmplt": "shortvideotplt"
    })
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    # soup = BeautifulSoup(resp.content)
    irs01_cookie = resp.cookies
    logger.info('RESP  %s - cookie:%s', url, dict(irs01_cookie))

    # TODO: baidu
    # url = 'http://api.share.baidu.com/s.gif?l=%s' % iqiyi_video_url

    # cp2.gif
    # example: http://msg.71.am/cp2.gif?p=v&t=s&lc=http%3A%2F%2Fm.iqiyi.com%2Fw_19rrtl7yh5.html&x=http%3A%2F%2Fstatic.iqiyi.com%2Fjs%2Fcommon%2Fares-4-0-10-7faec3ca17abc629c297.min.js&e=8e305b82a53f382eb1f3a638a3737ea3&y=qc_100001_100186&u=e3a08c4942b6550f8b6600e1706aa24c&av=4.0.10-ares3&g=0&d=112&v=4175004709&b=4175004709&c=30&pp=&pl=0&s=1500169416131
    # no cookie
    # @y = platform, source: http://static.qiyi.com/js/html5/js/page/playMovie/9f1cff6b4c!app.js
    # play:{ios:"qc_100001_100102",android:"qc_100001_100186",weixin:{ios:"qc_105092_300415",android:"qc_105092_300415"}}
    # 's': str(int(config['start_time'] / 1000))  # ?
    s = str(int(config['start_time']) + 1000)

    url = "http://msg.71.am/cp2.gif?%s" % urlencode({
        'p': 'v',
        't': 's',
        'lc': iqiyi_video_url,
        'x': 'http://static.iqiyi.com/js/common/ares-4-0-10-7faec3ca17abc629c297.min.js',
        'e': config['video_event_id'],  # ?
        'y': config['platform_code'],  # platform
        'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',  #uid
        'av': '4.0.10-ares3',  # from:
        'g': '0',  # ?
        'd': config_playinfo['duration'],  # "112"
        'v': config_playinfo['aid'],  # '4175004709',
        'b': config_playinfo['aid'],  # '4175004709',
        'c': config_playinfo['cid'],  # ?
        'pp': '',
        'pl': '0',  # ?
        's': s  # ?
    })
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    # soup = BeautifulSoup(resp.content)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # detailed info page
    # http://t7z.cupid.iqiyi.com/show2?e=AF48RQoBC1NFZQEWQF4NA1cWMwxYRERAC3AeXR5YQVsICnFSX10fR25uCEJCRVwFCAtqH1hEXVwXPQwEAQYFAkFXaAEJFlwNBW4GBQABBAVBWnlVDQEBAhcpDAAWQUYPVxMtDAAWQ0YMax8AHgAAHxAROkIDFldEDG4XVVEMARQHBjZVDQdRCFA5UFECBVYBRgBmVwYEUwNXOgNUBglWAhJaOQkBFkBEDG8XQEMMAA%3D%3D&h=1500218911636&a=qc_100001_100102&u=5e3fe8ea4becb9d14586d3d9e44560a5&p=&s=e09bc861c31b8dc8e03c221944b777c9
    encrypt_str = 'q=ct:1;ct:0&po=1&ul={video_url}&b={tvid}&l={tvid}&d={duration}&v=0&pv=&pr=0&sv={sv}&gt=1&ea=1&veid={veid}&pt=0&ps=0'.format(
        video_url=iqiyi_video_url,
        tvid=config_playinfo['tvid'],
        duration=config_playinfo['duration'],
        sv='4.0.10-ares3',
        veid=config['video_event_id'],  # video event id
        # veid='5753cc09e75aba09579d0e5f9d1ac20c',
    )

    # print(encrypt_str)
    # exit()

    # encrypt_str = 'q=ct:1;ct:0&po=1&ul=http://m.iqiyi.com/w_19rrtl7yh5.html&b=4175004709&l=4175004709&d=112&v=0&pv=&pr=0&sv=4.0.10-ares3&gt=1&ea=1&veid=5753cc09e75aba09579d0e5f9d1ac20c&pt=0&ps=0'
    # http://m.iqiyi.com/w_19rrtl7yh5.html&b=4175004709&l=4175004709&d=112&v=0&pv=&pr=0&sv=4.0.10-ares3&gt=1&ea=1&veid=5753cc09e75aba09579d0e5f9d1ac20c&pt=0&ps=0
    # cookie 3
    seed = 'qc_100001_100186'  # android
    encrypted = encrypt(encrypt_str, seed).decode('utf-8')
    h = str(int(config['start_time']) + random.randint(643, 983))
    url = 'http://t7z.cupid.iqiyi.com/show2?%s' % urlencode({
        'e': encrypted,
        'h': h,
        'a': seed,
        'u': new_QC006,  # '5e3fe8ea4becb9d14586d3d9e44560a5',
        'p': '',
        's': config['rid'],  # ?
    })

    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
            "QC006": old_cookie['QC006'],
            "QC112": old_cookie['QC112'],
            "QC007": old_cookie['QC007'],
            "QC008": old_cookie['QC008'],
            "T00404": old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            'play_stream': old_cookie['play_stream'],
            '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies = cookie, timeout=10)
        cupid_cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cupid_cookie))
    else:
        cookie = {
            "QC006": new_cookie['QC006'], #new_QC006
            "QC112": new_cookie['QC112'],# config['weid'],
            "QC007": new_cookie['QC007'],# "DIRECT"
            "QC008": new_cookie['QC008'] #config['weid']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    # soup = BeautifulSoup(resp.content)
    cupid_cookie = resp.cookies
    new_cookie['T00404'] = dict(cupid_cookie)['T00404']
    logger.info('RESP %s - cookie:%s', url, dict(cupid_cookie))
    config_show2 = resp.json()
    with open('config_show2.json', 'w', encoding='utf-8') as f:
        json.dump(config_show2, f, indent=2, ensure_ascii=False)
    # ad_list = config_show2['s']['a']


    # before ad
    # cp2.gif
    # example: http://msg.71.am/cp2.gif?p=v&t=s&lc=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&x=http%3A%2F%2Fstatic.iqiyi.com%2Fjs%2Fcommon%2Fares-4-0-10-7faec3ca17abc629c297.min.js&e=840a065d8fb4400fd0c81241ca6dad48&y=qc_100001_100186&u=110acaed85e6283ba09e42a05181dbbf&av=4.0.10-ares3&g=0&d=405&v=8216139609&b=8216139609&c=13&pp=&pl=0&s=1500781285409
    # "s": str(int(config['start_time'] / 1000))
    s = str(int(h) - random.randint(1, 5))
    url = "http://msg.71.am/cp2.gif?%s" % urlencode({
        "p": "v",
        "t": "s",
        "lc": config['url'],
        "x": "http://static.iqiyi.com/js/common/ares-4-0-10-7faec3ca17abc629c297.min.js",
        "e": config['video_event_id'],
        "y": config['platform_code'],
        "u": config['user_id'],
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "v": config_playinfo['aid'],
        "b": config_playinfo['aid'],
        "c": config_playinfo['cid'],
        "pp": "",
        "pl": "0",
        "s": s
    })
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    # soup = BeautifulSoup(resp.content)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # "s": str(int(config['start_time'] / 1000 + 2)),
    if not config_playinfo['sid']:
        b = config_playinfo['aid']
    else:
        b = config_playinfo['sid']
    s = str(int(h) - random.randint(86, 430) - random.randint(30, 40))
    url = "http://msg.71.am/cp2.gif?%s" % urlencode({
        "p": "s",
        "rd": "186",  # ?变化
        "rc": "-1",
        "t": "s",
        "e": config['video_event_id'],
        "y": config['platform_code'],
        "u": config['user_id'],
        "a": cupid_cookie['T00404'],
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": config['rid'],
        "s": s,
        "b": b,
        "c": config_playinfo['cid'],
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": config_show2['pbp']['qxlc'],
        "ps": "0"
    })
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    # soup = BeautifulSoup(resp.content)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))


    for each_s in config_show2['s']:
        for each_a in each_s['a']:
            ai = '{ctt}||{dsi}||{ori}||{cra}||{tt}'.format(
                ctt=each_a.get('ctt', '0'),
                dsi=each_a.get('dsi', ''),
                ori=each_a.get('ori', ''),
                cra=each_a.get('cr', {}).get('cra', ''),
                tt=each_a.get('cr', {}).get('tt', ''),
            )
            # http://msg.71.am/cp2.gif?p=c&t=l&e=840a065d8fb4400fd0c81241ca6dad48&y=qc_100001_100186&u=110acaed85e6283ba09e42a05181dbbf&a=fcd89175e66a94c39bab1b9f608c7e7f&av=4.0.10-ares3&g=0&d=405&rid=77819a85df68d1567aca638d63cdc023&ai=0%7C%7C71000028%7C%7C81000019%7C%7C54001042280%7C%7Crelatedapp&s=1500781285043&c=13&h=0&l=MzYuMTAyLjIyOC4xMDM%3D&qxlc=86150000&ps=0
            #  "s": str(int(config['start_time'] / 1000 + 3)),
            s = str(int(h) - random.randint(86, 430))
            url = "http://msg.71.am/cp2.gif?%s" % urlencode({
                "p": "c",
                "t": "l",
                "e": config['video_event_id'],
                "y": config['platform_code'],
                "u": config['user_id'],
                "a": cupid_cookie['T00404'],
                "av": "4.0.10-ares3",
                "g": "0",
                "d": config_playinfo['duration'],
                "rid": config['rid'],
                "ai": ai,
                "s": s,
                "b": b,
                "c": config_playinfo['cid'],
                "h": "0",
                "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
                "qxlc": config_show2['pbp']['qxlc'],
                "ps": "0"
            })
            logger.info('GET  %s', url)
            resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
            # soup = BeautifulSoup(resp.content)
            cookie = resp.cookies
            logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 0721 add after http://msg.71.am/cp2.gif
    url = 'http://msg.71.am/tmpstats.gif?type=piaoshhtestmayttf&des=h5p2ptest&mse=1&p2p=1&p=h5'
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 67 89 49 95 6
    # http://msg.71.am/b?t=21&bstp=0&pf=2&p=20&p1=201&u=658563ea82a863eeaf3b7ebcc9c970e4&pu=&block=612091&rn=678949956&c1=13&tmplt=shortvideotplt
    rn = ''.join(['%s' % random.randint(0, 9) for num in range(0, 9)])
    url = 'http://msg.71.am/b?%s' % urlencode({
        "t": "21",
        "bstp": "0",
        "pf": "2",
        "p": "20",
        "p1": "201",
        "u": config['user_id'],
        "pu": "",
        "block": "605251_episode",#"612091",
        "rn": rn,
        "c1": config_playinfo['cid'],
        "tmplt": "longsourcetplt"#"shortvideotplt"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # http://msg.71.am/b?t=21&bstp=0&pf=2&p=20&p1=201&u=658563ea82a863eeaf3b7ebcc9c970e4&pu=&block=605251_sub&rn=71055104&c1=13&tmplt=shortvideotplt
    # rn使用随机数共8位 71 05 51 04
    rn = ''.join(['%s' % random.randint(0, 9) for num in range(0, 8)])
    url = 'http://msg.71.am/b?%s' % urlencode({
        "t": "21",
        "bstp": "0",
        "pf": "2",
        "p": "20",
        "p1": "201",
        "u": config['user_id'],
        "pu": "",
        "block": "605251_sub",
        "rn": rn,
        "c1": config_playinfo['cid'],
        "tmplt": "longsourcetplt"#"shortvideotplt"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # 和周围的相关
    # url = http://msg.71.am/tmpstats.gif?type=140704yyzx&pf=2&p=20&p1=201&p2=2011&u=110acaed85e6283ba09e42a05181dbbf&pu=&rn=1500857626744&serverid=h5recomd&qpid=212289620&appname=%E7%BE%8E%E8%B6%A3%E6%B6%88%E7%81%AD%E6%98%9F%E6%98%9F&apptype=2&frsale=1&t=21&serverid1=convide&block=50805_1
    # 时间戳比这个链接多2000;http://iqiyi.irs01.com/irt?_iwt_t=
    rn = str(int(config['start_time']) + random.randint(60, 105) + 2000)
    url = 'http://msg.71.am/tmpstats.gif?%s' % urlencode({
        "type": "140704yyzx",
        "pf": "2",
        "p": "20",
        "p1": "201",
        "p2": "2011",
        "u": config['user_id'],
        "pu": "",
        "rn": rn,
        "serverid": "h5recomd",
        "qpid": "212131820",  # 广告app相关的id
        "appname": "美趣消灭星星",  # 广告app名字
        "apptype": "2",
        "frsale": "1",
        "t": "21",
        "serverid1": "convide",
        "block": "50805_1"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # # http://msg.video.qiyi.com/tmpstats.gif?usract=show&event_id=0d022f6a0a0f8390625f9ce092765987&bkt=h_swan_main_default&area=h_swan&tag=&aid=8216139609&cid=13&rank=&taid=&tcid=&type=recctplay20121226&ppuid=&uid=766b48186074c2f51d607a50b4f66963&albumlist=8147553709,8187060809,7813432509,544928609,7699263509,7590326709,113246000,7844901009,692913300,8179876909,8127257909,714790200,596215300,725794400,353488100&url=&t=1500943072827&platform=31
    # # event_id使用随机字符串代替
    # # albumlist为视频播放下面的视频列表
    # t = str(int(config['start_time']) + random.randint(60, 105) + 2100)
    # url = 'http://msg.video.qiyi.com/tmpstats.gif?%s' % urlencode({
    #     "usract": "show",
    #     "event_id": ''.join(random.choice(string.digits + string.ascii_lowercase) for _ in range(32)),
    #     "bkt": "h_swan_main_default",
    #     "area": "h_swan",
    #     "tag": " ",
    #     "aid": config_playinfo['aid'],
    #     "cid": config_playinfo['cid'],
    #     "rank": " ",
    #     "taid": " ",
    #     "tcid": " ",
    #     "type": "recctplay20121226",
    #     "ppuid": " ",
    #     "uid": config['user_id'],
    #     "albumlist": "8147553709,8187060809,7813432509,544928609,7699263509,7590326709,113246000,7844901009,692913300,8179876909,8127257909,714790200,596215300,725794400,353488100",
    #     "url": "",
    #     "t": t,
    #     "platform": "31"
    # })
    # resp = requests.get(url, headers=headers(base_headers(url, ua)), timeout=10)
    # cookie = resp.cookies
    # logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 在点击播放之前,有9个http://msg.71.am/cp2.gif?格式的请求
    # 1.http://msg.71.am/cp2.gif?p=qxt&t=s&rc=-1&rd=166&ai=0%7C%7C71000028%7C%7C81000019%7C%7C54000777755%7C%7Crelatedapp&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&s=1500943071993&c=13&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    rdbase = str(random.randint(24, 225))
    rand1 = random.randint(54000777748, 54000777755)
    ai = "0" + "||" + "71000028" + "||" + "81000019" + \
        "||" + "%s" % rand1 + "||" + "relatedapp"
    e = ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(32))
    a = ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(32))
    rid = ''.join(random.choice(string.ascii_lowercase + string.digits)
                for _ in range(32))
    # b = playInfo.sid
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "p": "qxt",
        "t": "s",
        "rc": "-1",
        "rd": rdbase,
        "ai": ai,
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 1300),
        "b": b,
        "c": config_playinfo['cid'],
        "h": 0,
        "l": "MTEwLjk2LjIyNS4yMzM=",
        "qxlc": "86110108",
        "ps": "0"

    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    logger.info('baidu 2')
    # cookie 4
    # http://hm.baidu.com/hm.js?5df871ab99f94347b23ca224fc7d013f
    url = 'http://hm.baidu.com/hm.js?5df871ab99f94347b23ca224fc7d013f'
    logger.info('GET  %s', url)

    if use_old_cookie:
        cookie = {
        'BAIDUID': old_cookie['BAIDUID'],
        'HMACCOUNT': old_cookie['HMACCOUNT']
        }
        logger.info('GET  %s', url)
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        humcout = resp.cookies
        logger.info('RESP  %s - cookie:%s', url, dict(humcout))
    else:
        cookie = {
            'BAIDUID': new_cookie['BAIDUID']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        humcout = resp.cookies
        new_cookie['HMACCOUNT'] = dict(humcout)['HMACCOUNT']
        logger.info(new_cookie['HMACCOUNT'])
        logger.info('RESP  %s - cookie:%s', url, dict(humcout))


    logger.info('baidu 3')
    # cookie 5
    # http://hm.baidu.com/hm.gif?cc=0&ck=1&cl=24-bit&ds=384x640&et=0&ja=0&ln=en-us&lo=0&nv=1&rnd=1856549513&si=5df871ab99f94347b23ca224fc7d013f&st=1&v=1.2.16&lv=1&ct=!!&tt=%E7%BA%A6%E4%BC%9A%E5%B0%B1%E6%98%AF%E8%A6%81%E4%B8%8D%E4%B8%80%E6%A0%B7%E7%9A%84%E5%A6%86%E5%AE%B9-%E6%97%B6%E5%B0%9A-%E9%AB%98%E6%B8%85%E8%A7%86%E9%A2%91%E2%80%93%E7%88%B1%E5%A5%87%E8%89%BA&sn=57594
    rn1 = ''.join(['%s' % random.randint(0, 9) for x in range(9)])
    rnd = '1' + rn1
    sn = str(random.randint(38552, 60649))
    resp = requests.get(iqiyi_video_url)
    tt = BeautifulSoup(resp.text, 'lxml').title.string
    url = 'http://hm.baidu.com/hm.gif?%s' % urlencode({
        "cc ": "0",
        "ck ": "1",
        "cl ": "24-bit",
        "ds ": "384x640",
        "et ": "0",
        "ja ": "0",
        "ln ": "zh-cn",
        "lo ": "0",
        "nv ": "1",
        "rnd ": rnd,
        "si ": "5df871ab99f94347b23ca224fc7d013f ",
        "st ": "1",
        "v ": "1.2.16 ",
        "lv ": "1",
        "ct ": "!!",
        "tt ": tt,
        "sn ": sn
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
        'BAIDUID': old_cookie['BAIDUID'],
        'HMACCOUNT': old_cookie['HMACCOUNT']
        }
        logger.info('GET  %s', url)
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP  %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
            'BAIDUID': new_cookie['BAIDUID'],
            'HMACCOUNT': new_cookie['HMACCOUNT']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP  %s - cookie:%s', url, dict(cookie))


    logger.info('baidu 4')
    # cookie 6
    # http://datax.baidu.com/x.gif?he=%5bmagic_num%3d1213018161%26prot_ver%3d1%26app_id%3d1002%26rnd%3d0%26log_format%3d0%26encrypt_choose%3d0%5d&si=5df871ab99f94347b23ca224fc7d013f&dm=m.iqiyi.com&ac=c9b0caaae57cc1c41f9fbce47cc171f2&v=1.0.0&li=1524358158&rnd=1501202304345
    li = ''.join(['%s' % random.randint(1, 9) for x in range(9)])
    rnd = str(int(config['start_time']) + 3500)
    url = 'http://datax.baidu.com/x.gif?%s' % urlencode({
        "he": "[magic_num=1213018161&prot_ver=1&app_id=1002&rnd=0&log_format=0&encrypt_choose=0]",
        "si": "5df871ab99f94347b23ca224fc7d013f",
        "dm": "m.iqiyi.com",
        "ac": "c9b0caaae57cc1c41f9fbce47cc171f2",
        "v": "1.0.0",
        "li": li,
        "rnd": rnd
    })
    if use_old_cookie:
        cookie = {
            'BAIDUID': old_cookie['BAIDUID'],
        }
        reps = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
            'BAIDUID': new_cookie['BAIDUID'],
        }
        reps = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # 20170725add.请求发生的点击播放之前
    # cookie 7
    # url =
    # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&rfr=&msrc=&vfm=&ve=bf59ac625b4d1e68fe199e650b4a9b5b&ce=48cded8e4ea86dc90bb661957db5ef08&r=8216139609&vfrm=&u=110acaed85e6283ba09e42a05181dbbf&nu=0&pu=&rn=1500856972490&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=e778572d8ffe63b8d2e0572152a62ba4&qdv=1&t=15&vid=6eed636cb58c5c822172f51f53a05f91&c1=13&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1
    randomsigC = ''.join(random.choice(
        string.ascii_lowercase + string.digits) for _ in range(32))
    rn = str(int(config['start_time']) + random.randint(60, 105) + 1000)
    url = 'http://msg.iqiyi.com/b?%s' % urlencode({
        's1': '1',
        's2': iqiyi_video_url,
        'rfr': '',
        'msrc': '',
        'vfm': '',
        've': config['video_event_id'],  # ?
        'ce': config['weid'],  # weid, cookie(QC008)
        'r': config_playinfo['tvid'],  # '4175004709',
        'vfrm': '',
        'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
        'nu': nu,
        'pu': '',
        'rn': rn,
        'os': 'android',
        'ft': 'mp4',
        'lrfr': 'DIRECT',
        'openid': '',
        'pf': '2',
        'p': '20',
        'p1': '201',
        'p2': '1011',
        #'__sigC': randomsigC,
        #"qdv": "1",
        't': '15',
        'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
        'c1': config_playinfo['cid'],
        'upderid': '',
        'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
        'hu': '-1'
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
            'QC006': old_cookie['QC006'],
            'QC112': old_cookie['QC112'],
            'QC007': old_cookie['QC007'],
            'QC008': old_cookie['QC008'],
            'T00404': old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            '__dfp': old_cookie['dfp'],
            'play_stream': old_cookie['play_stream']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
            'QC006': new_cookie['QC006'],
            'QC112': new_cookie['QC112'],
            'QC007': new_cookie['QC007'],
            'QC008': new_cookie['QC008'],
            'T00404': new_cookie['T00404'],
            '__uuid': new_cookie['uuid'],
            '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # 2
    # http://msg.71.am/cp2.gif?p=c&t=l&x=g&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&ai=1%7C%7C71000028%7C%7C81000019%7C%7C54000947893%7C%7Croll&s=1500943077529&c=13&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    rand2 = random.randint(54000947893, 54001009432)
    ai = "1" + "||" + "71000028" + "||" + "81000019" + \
        "||" + "%s" % rand2 + "||" + "roll"
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "p": "c",
        "t": "l",
        "x": "g",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "ai": ai,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 6300),
        "b": b,
        "c": config_playinfo['cid'],
        "h": "0",
        "l": "MTEwLjk2LjIyNS4yMzM=",
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 20170813 add miaozhen
    # http://tyaqy.m.cn.miaozhen.com/x/k=2052352&p=77Dt0&dx=__IPDX__&rt=2&ns=110.96.228.67&ni=IQY_5000000139041&v=__LOC__&xa=__ADPLATFORM__&tr=__REQUESTID__&o=
    url = 'http://tyaqy.m.cn.miaozhen.com/x/%s' %urlencode({
        "k": "2052352",
        "p": "77Dt3",
        "dx": "__IPDX__",
        "rt": "2",
        "ns": "110.96.228.67",
        "ni": "IQY_5000000139041",
        "v": "__LOC__",
        "xa": "__ADPLATFORM__",
        "tr": "__REQUESTID__",
        "o": ""
    })
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), allow_redirects=True, timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 20170816add admaster
    # http://397c0.admaster.com.cn/i/a91005,b1900186,c1118,i0,m202,8a2,8b1,0iPUB_5000000127187,h
    url = 'http://397c0.admaster.com.cn/i/a91005,b1900186,c1118,i0,m202,8a2,8b1,0iPUB_5000000127187,h'
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), allow_redirects=True, timeout=10, verify=False)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 3
    # http://msg.71.am/cp2.gif?p=i&t=s&oi=1%3Ap1%7C2%7C3%7C4%7C5%3A1000000005255%3B0%3An1%3A1000000005812%3B0%3An1%3A1000000006544%3B&ri=1%3Ap1%7C2%7C3%7C4%7C5%3A1000000005255%3B0%3An1%3A1000000005812%3B0%3An1%3A1000000006544%3B&di=0%3An1%2C73%2C%2C2022%3A1000000005812%3B1%3Ap2%2C73%2C81000019%2C2026%3A1000000005255%3B&fi=1%3Ap5%7C4%7C3%3A1000000005255%3B&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&v=8216139609&b=0&c=13&s=1500943077540&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "p": "i",
        "t": "s",
        "oi": "1:p1|2|3|4|5:1000000005255;0:n1:1000000005812;0:n1:1000000006544",
        "ri": "1:p1|2|3|4|5:1000000005255;0:n1:1000000005812;0:n1:1000000006544",
        "di": "0:n1,73,,2022:1000000005812;1:p2,73,81000019,2026:1000000005255",
        "fi": "1:p5|4|3:1000000005255",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "v": config_playinfo['tvid'],#"8216139609"
        "b": b,
        "c": config_playinfo['cid'],
        "s": str(int(config['start_time']) + random.randint(60, 105) + 6311),
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 4
    # http://msg.71.am/cp2.gif?t=st&p=a&od=81000019&ct=54000947893&dp=71000028&as=1%3A2%3A2&x=rpt%3A0&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&v=8216139609&b=0&c=13&ai=1%7C%7C71000028%7C%7C81000019%7C%7C54000947893%7C%7Croll&s=1500943077542&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "t": "st",
        "p": "a",
        "od": "81000019",
        "ct": str(rand2),
        "dp": "71000028",
        "as": "1:2:2",
        "x": "rpt:0",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "v": config_playinfo['tvid'],
        "c": config_playinfo['cid'],
        "b": b,
        "ai": ai,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 6313),
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 20170816 add 302
    url = 'http://m.reachmax.cn/ad.gif?aid=s231a8edebdffb949d2'
    logger.info('GET  %s', url)
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), allow_redirects=True, timeout=10, verify=False)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 5
    # http://msg.71.am/cp2.gif?p=t&t=s&rc=-1&rd=89&ai=1%7C%7C%7C%7C%7C%7C%7C%7C&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&s=1500943077624&c=13&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    rd1 = str(random.randint(24, 225))
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "p": "t",
        "t": "s",
        "rc": "-1",
        "rd": str(rd1),
        "ai": "1||||||||",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 6363),
        "b": b,
        "c": config_playinfo['cid'],
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 20170816 add
    # http://msg.71.am/cp2.gif?p=t&t=s&rc=-1&rd=801&ai=1%7C%7C71000001%7C%7C5000000983923%7C%7C5000001069696%7C%7Croll&e=7bab99599a6ec38da369f34590f358b2&y=qc_100001_100186&u=49f1a99134df0cb8f081b5e24e5e3d57&a=6a7737f3a21bc493bb0c74166535040f&av=4.0.10-ares3&g=0&d=207&rid=b26c7892f9f41b048f1b5e8415d5f59d&s=1502864594726&b=207270801&c=12&h=0&l=MTEwLjk2LjE5MC44&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "p": "t",
        "t": "s",
        "rc": "-1",
        "rd": rd1,
        "ai": ai,
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 6363),
        "b": b,
        "c": config_playinfo['cid'],
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 6
    # http://msg.71.am/cp2.gif?t=1q&p=a&od=81000019&ct=54000947893&dp=71000028&as=1%3A2%3A2&x=rpt%3A0&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&v=8216139609&b=0&c=13&ai=1%7C%7C71000028%7C%7C81000019%7C%7C54000947893%7C%7Croll&s=1500943080530&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "t": "1q",
        "p": "a",
        "od": "81000019",
        "ct": str(rand2),
        "dp": "71000028",
        "as": "1:2:2",
        "x": "rpt:0",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "v": config_playinfo['tvid'],
        "b": b,
        "c": config_playinfo['cid'],
        "ai": ai,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 8363),
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # 7
    # http://msg.71.am/cp2.gif?t=mid&p=a&od=81000019&ct=54000947893&dp=71000028&as=1%3A2%3A2&x=rpt%3A0&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&v=8216139609&b=0&c=13&ai=1%7C%7C71000028%7C%7C81000019%7C%7C54000947893%7C%7Croll&s=1500943084781&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "t": "mid",
        "p": "a",
        "od": "81000019",
        "ct": str(rand2),
        "dp": "71000028",
        "as": "1:2:2",
        "x": "rpt:0",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "v": config_playinfo['tvid'],
        "b": b,
        "c": config_playinfo['cid'],
        "ai": ai,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 12363),
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # 8
    # http://msg.71.am/cp2.gif?t=3q&p=a&od=81000019&ct=54000947893&dp=71000028&as=1%3A2%3A2&x=rpt%3A0&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&v=8216139609&b=0&c=13&ai=1%7C%7C71000028%7C%7C81000019%7C%7C54000947893%7C%7Croll&s=1500943087529&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "t": "3q",
        "p": "a",
        "od": "81000019",
        "ct": str(rand2),
        "dp": "71000028",
        "as": "1:2:2",
        "x": "rpt:0",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "v": config_playinfo['tvid'],
        "b": b,
        "c": config_playinfo['cid'],
        "ai": ai,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 14363),
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # 9
    # http://msg.71.am/cp2.gif?t=sp&p=a&od=81000019&ct=54000947893&dp=71000028&as=1%3A2%3A2&x=rpt%3A0&e=66b252e08f66890dcea0882cab2333fc&y=qc_100001_100186&u=766b48186074c2f51d607a50b4f66963&a=307cd6fafe06643ec91afdcd1b646565&av=4.0.10-ares3&g=0&d=405&rid=cd95c3da47badb77bd6ee7cb29974c37&v=8216139609&b=0&c=13&ai=1%7C%7C71000028%7C%7C81000019%7C%7C54000947893%7C%7Croll&s=1500943091778&h=0&l=MTEwLjk2LjE4OC40NQ%3D%3D&qxlc=86110108&ps=0
    url = 'http://msg.71.am/cp2.gif?%s' % urlencode({
        "t": "sp",
        "p": "a",
        "od": "81000019",
        "ct": str(rand2),
        "dp": "71000028",
        "as": "1:2:2",
        "x": "rpt:0",
        "e": e,
        "y": "qc_100001_100186",
        "u": config['user_id'],
        "a": a,
        "av": "4.0.10-ares3",
        "g": "0",
        "d": config_playinfo['duration'],
        "rid": rid,
        "v": config_playinfo['tvid'],
        "b": b,
        "c": config_playinfo['cid'],
        "ai": ai,
        "s": str(int(config['start_time']) + random.randint(60, 105) + 18363),
        "h": "0",
        "l": b64encode(ip.encode('utf-8')).decode('utf-8'),
        "qxlc": "86110108",
        "ps": "0"
    })
    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), timeout=10)
    cookie = resp.cookies
    logger.info('RESP %s - cookie:%s', url, dict(cookie))

    print('getvplay')
    # happens after click play
    # getvplay
    # Zepto1500169415738是一个时间轴的起始位置，通过对比视频播放过程中出现的全部和时间相关的内容，这里的值最小现在修改程序当中和时间先关的位置
    # http://l.rcd.iqiyi.com/apis/qiyirc/getvplay?tvId=4175004709&agent_type=13&_=1500169510982&callback=Zepto1500169415738
    # 'callback': 'Zepto%s' % (''.join(random.choice(string.digits) for _ in range(13)))
    # '_': ''.join(random.choice(string.digits) for _ in range(13)),
    # cookie 8
    Zepto = str(int(config['start_time']))  # 13位时间戳
    oldbasetime = str(int(config['start_time']) +
                    random.randint(21000, 30000))  # 和Zepto相差21000~30000
    url = 'http://l.rcd.iqiyi.com/apis/qiyirc/getvplay?%s' % urlencode({
        'tvId': config_playinfo['tvid'],  # '4175004709',
        'agent_type': '13',
        '_': oldbasetime,
        'callback': 'Zepto%s' % Zepto
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
            'QC006': old_cookie['QC006'],
            'QC112': old_cookie['QC112'],
            'QC007': old_cookie['QC007'],
            'QC008': old_cookie['QC008'],
            'T00404': old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            '__dfp': old_cookie['dfp'],
            'play_stream': old_cookie['play_stream']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
            'QC006': new_cookie['QC006'],
            'QC112': new_cookie['QC112'],
            'QC007': new_cookie['QC007'],
            'QC008': new_cookie['QC008'],
            'T00404': new_cookie['T00404'],
            '__uuid': new_cookie['uuid'],
            '__dfp': new_cookie['dfp'],
            'play_stream': new_cookie['play_stream']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))

        cookie = dict(cupid_cookie)
        cookie.update({'QC006': new_QC006, 'QC007': 'DIRECT', 'play_stream': '1'})

    r_random = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(32))
    count = 1
    for each_s in config_show2['s']:
        st = each_s['st']
        ea = each_s['ea']
        if 'tts' in ea:
            tts = ea['tts']
            for each_tts in tts:
                if 'c' in each_tts:
                    c = each_tts['c']
                else:
                    continue
                if 'dts' in each_tts and dts is None:
                    dts = each_tts['dts']
                else:
                    continue

                # args example:
                # w:1,2,3,4,5
                # dts:1:1001:,2:2002:2023,3:1002:,4:1002:,5:1002:
                # nr:
                # c:3adf0340b936e47ada4ca1a40e572a63
                # f:b59d48f97de554b5e9a37ed1a7cba7de
                # g:ecc4921c16b72ba30353fc924a4cf476
                # h:
                # i:qc_100001_100186
                # j:30
                # n:331529200
                # ri:adf275092ad13740184788ed4499db20
                # o:1
                # t:1000000005255
                # p:1000000000381
                # dr:136
                # as:1;2;3,4,5
                # rt:1500223794
                # d:1000000000071
                # a:0
                # kp:b59d48f97de554b5e9a37ed1a7cba7de
                # iv:0
                # ivm:0
                # ve:1
                # sv:4.0.10-ares3
                # r:cede026a5eab51aa5627e8029c18c27f
                # st:0
                # b:1500223806
                # s:1755d120384c9ca5abfa53ca7e9ce7c3
                # cb:jQuery112409334110235846977_1500221971521
                # _:1500221971523
                args = dict(each_s['ea']['trp'])
                args.update(each_tts)
                args.update({
                    "ve": "1",
                    "sv": "4.0.10-ares3",
                    "r": r_random,  # "cede026a5eab51aa5627e8029c18c27f"
                    "st": st,
                    "b": str(int(config['start_time'] / 1000 + index * 15)),
                    "s": "1755d120384c9ca5abfa53ca7e9ce7c3",  # ?
                    "cb": "jQuery112409334110235846977_1500221971521",
                })
                # cookie 9
                url = 'http://t7z.cupid.iqiyi.com/track2?%s' % urlencode(args)
                logger.info('GET  %s', url)
                if use_old_cookie:
                    cookie = {
                        'QC006': old_cookie['QC006'],
                        'QC112': old_cookie['QC112'],
                        'QC007': old_cookie['QC007'],
                        'QC008': old_cookie['QC008'],
                        'T00404': old_cookie['T00404'],
                        '__uuid': old_cookie['uuid'],
                        '__dfp': old_cookie['dfp'],
                    }
                    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
                # soup = BeautifulSoup(resp.content)
                    cookie = resp.cookies
                    logger.info('RESP %s - cookie:%s', url, dict(cookie))
                else:
                    cookie = {
                    'QC006': new_cookie['QC006'],
                    'QC112': new_cookie['QC112'],
                    'QC007': new_cookie['QC007'],
                    'QC008': new_cookie['QC008'],
                    'T00404': new_cookie['T00404'],
                    '__uuid': new_cookie['uuid'],
                    '__dfp': new_cookie['dfp']
                    }
                    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
                    # soup = BeautifulSoup(resp.content)
                    cookie = resp.cookies
                    logger.info('RESP %s - cookie:%s', url, dict(cookie))

        ad_end_timestamp = int(h)
        ad_list = each_s['a']
        for each in ad_list:
            if 'va' in each and 'vo' in each['va']:
                # TODO: 验证是否等于广告数，获取广告时长（这里假定都是15秒）
                ad_end_timestamp += 15000  # 加上15s广告

                ad_xml = each['va']['vo']
                soup = BeautifulSoup(ad_xml, "lxml-xml")
                logger.info('playing ad')
                imp = soup.find('Impression').text.strip()
                se = each['se']
                print('[[ ----------------')
                if imp:
                    url = imp.replace('${SETTLEMENT}', se)
                    logger.info('GET  %s', url)
                    resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
                    # soup = BeautifulSoup(resp.content)
                    cookie = resp.cookies
                    logger.info('RESP %s - cookie:%s', url, dict(cookie))
                # set_rate = 0.002 #
                click_condition = get_targetrate(click_ad_ratio)
                # if random.randint(0, 10) in (1, 2, 3):
                if click_condition:
                    # if True:
                    click = soup.find('ClickThrough').text
                    click_track = soup.find('ClickTracking').text
                    if click and click_track:
                        for url in (click, click_track):
                            url = url.replace('${SETTLEMENT}', se)
                            logger.info('GET  %s', url)
                            proc = None
                            try:
                                proc = multiprocessing.Process(target=lambda: requests.get(url, headers=headers(
                                    base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10))
                                proc.start()
                                # proc.join(10)
                            except BaseException as e:
                                logger.error('failed to start process init')
                            else:
                                pass
                            if proc is not None:
                                for _ in range(2): #广告等待请求
                                    if proc.is_alive():
                                        logger.debug('wait process')
                                        time.sleep(1)
                                    else:
                                        logger.info('process finished')
                                        break
                                else:
                                    proc.terminate()
                            else:
                                logger.error('failed to start process')
                                # soup = BeautifulSoup(resp.content)
                                # cookie = resp.cookies
                                # logger.info('RESP %s - cookie:%s', url, dict(cookie))
                print('---------------- ]]')


    # 开始播放，开始心跳
    # 第一次无tm
    # 第二次心跳15s, 然后60s
    # 往后均为120s一跳，不足120s按不足跳完
    # 跳完后补一发心跳，不带tm
    basern = str(int(oldbasetime) + random.randint(10, 20))
    # 'start_time': time.time() * 1000,
    # play after 1. no 'tm'
    # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rrtl7yh5.html&rfr=&msrc=&vfm=&ve=faf0c728031136fd62877e46b842271f&ce=134a8209ff557db9c226f757c0cbeaaf&r=4175004709&vfrm=&u=e3a08c4942b6550f8b6600e1706aa24c&nu=1&pu=&rn=1500169510958&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=deb107ae60d799d1be8f2f8591b1ca12&qdv=1&t=1&vid=a462923b370a4a880aef5d74bcef2079&c1=30&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1
    # 此处的rn比点击播放时候的url参数中的"_"少10~20
    # cookie 10
    logger.info('pre heartbeat %s', basern)
    url = 'http://msg.iqiyi.com/b?%s' % urlencode({
        's1': '1',
        's2': iqiyi_video_url,
        'frf': '',
        'msrc': '',
        'vfm': '',
        've': config['video_event_id'],  # ?
        'ce': config['weid'],  # weid, cookie(QC008)
        'r': config_playinfo['tvid'],  # '4175004709',
        'vfrm': '',
        'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
        'nu': nu,
        'pu': '',
        'rn': basern,  # 后面几个url里面含有tm参数的请求的rn时间基准以这个为基准
        'os': 'android',
        'ft': 'mp4',
        'lrfr': 'DIRECT',
        'openid': '',
        'pf': '2',
        'p': '20',
        'p1': '201',
        'p2': '1011',
        #'__sigC': randomsigC,
        #'qdv': '1',
        't': '1',
        'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
        'c1': config_playinfo['cid'],
        'upderid': '',
        'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
        'hu': '-1'
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
            'QC006': old_cookie['QC006'],
            'QC112': old_cookie['QC112'],
            'QC007': old_cookie['QC007'],
            'QC008': old_cookie['QC008'],
            'T00404': old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            'play_stream': old_cookie['play_stream'],
            '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
        'QC006': new_cookie['QC006'],
        'QC112': new_cookie['QC112'],
        'QC007': new_cookie['QC007'],
        'QC008': new_cookie['QC008'],
        'T00404': new_cookie['T00404'],
        '__uuid': new_cookie['uuid'],
        'play_stream': new_cookie['play_stream'],
        '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 视频播放之后相似5个请求中tm值15， 60， 120， 120， 90
    # play after 2
    # tm = 15
    # cookie 11
    # rn和没有tm的rn时间差random.randint(1510, 1530)
    # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rrtl7yh5.html&rfr=&msrc=&vfm=&ve=faf0c728031136fd62877e46b842271f&ce=134a8209ff557db9c226f757c0cbeaaf&r=4175004709&vfrm=&u=e3a08c4942b6550f8b6600e1706aa24c&nu=1&pu=&rn=1500169525986&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=deb107ae60d799d1be8f2f8591b1ca12&qdv=1&t=2&tm=15&vid=a462923b370a4a880aef5d74bcef2079&c1=30&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1

    heartbeat_count = 0
    duration = int(config_playinfo['duration'])
    rn_tm = []
    # 15
    left_duration = duration
    new_rn = int(basern)
    if duration > 15:
        rn = str(int(basern) + 15000 + random.randint(-100, 100))
        tm = 15
        rn_tm.append((rn, tm))
        left_duration -= 15
        new_rn += 15000
    if duration > 60:
        rn = str(int(basern) + 60000 + random.randint(-100, 100))
        tm = 60
        rn_tm.append((rn, tm))
        left_duration -= (60 - 15)
        new_rn += (60000 - 15000)

    while left_duration > 0:
        if left_duration > 120:
            tm = 120
        else:
            tm = left_duration - 15
        rn_offset = tm * 1000
        new_rn += rn_offset
        rn_tm.append((new_rn + random.randint(-100, 100), tm))
        left_duration -= 120

    # tm15rn = str(int(basern) + random.randint(1510, 1530))
    for rn, tm in rn_tm:
        logger.info('heartbeat %s - %s', tm, rn)
        url = 'http://msg.iqiyi.com/b?%s' % urlencode({
            's1': '1',
            's2': iqiyi_video_url,
            'rfr': '',
            'msrc': '',
            'vfm': '',
            've': config['video_event_id'],  # ?
            'ce': config['weid'],  # weid, cookie(QC008)
            'r': config_playinfo['tvid'],  # '4175004709',
            'vfrm': '',
            'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
            'nu': nu,
            'pu': '',
            'rn': rn,
            'os': 'android',
            'ft': 'mp4',
            'lrfr': 'DIRECT',
            'openid': '',
            'pf': '2',
            'p': '20',
            'p1': '201',
            'p2': '1011',
            #'__sigC': randomsigC,
            #'qdv': '1',
            't': '2',
            'tm': tm,
            'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
            'c1': config_playinfo['cid'],
            'upderid': '',
            'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
            'hu': '-1'
        })
        logger.info('GET  %s', url)
        if use_old_cookie:
            cookie = {
            'QC006': old_cookie['QC006'],
            'QC112': old_cookie['QC112'],
            'QC007': old_cookie['QC007'],
            'QC008': old_cookie['QC008'],
            'T00404': old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            'play_stream': old_cookie['play_stream'],
            '__dfp': old_cookie['dfp']
            }
            resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
            cookie = resp.cookies
            logger.info('RESP %s - cookie:%s', url, dict(cookie))
        else:
            cookie = {
            'QC006': new_cookie['QC006'],
            'QC112': new_cookie['QC112'],
            'QC007': new_cookie['QC007'],
            'QC008': new_cookie['QC008'],
            'T00404': new_cookie['T00404'],
            '__uuid': new_cookie['uuid'],
            'play_stream': new_cookie['play_stream'],
            '__dfp': new_cookie['dfp']
            }
            resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
            cookie = resp.cookies
            logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        rn = rn + random.randint(10, 100)
        tm120rn2 = str(rn)
        logger.info('heartbeat LAST - %s', rn)
        url = 'http://msg.iqiyi.com/b?%s' % urlencode({
            's1': '1',
            's2': iqiyi_video_url,
            'rfr': '',
            'msrc': '',
            'vfm': '',
            've': config['video_event_id'],  # ?
            'ce': config['weid'],  # weid, cookie(QC008)
            'r': config_playinfo['tvid'],  # '4175004709',
            'vfrm': '',
            'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
            'nu': nu,
            'pu': '',
            'rn': rn,
            'os': 'android',
            'ft': 'mp4',
            'lrfr': 'DIRECT',
            'openid': '',
            'pf': '2',
            'p': '20',
            'p1': '201',
            'p2': '1011',
            #'__sigC': randomsigC,
            #'qdv': '1',
            't': '2',
            'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
            'c1': config_playinfo['cid'],
            'upderid': '',
            'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
            'hu': '-1'
        })
        logger.info('GET  %s', url)
        if use_old_cookie:
            cookie = {
            'QC006': old_cookie['QC006'],
            'QC112': old_cookie['QC112'],
            'QC007': old_cookie['QC007'],
            'QC008': old_cookie['QC008'],
            'T00404': old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            'play_stream': old_cookie['play_stream'],
            '__dfp': old_cookie['dfp']
            }
            resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
            cookie = resp.cookies
            logger.info('RESP %s - cookie:%s', url, dict(cookie))
        else:
            cookie = {
            'QC006': new_cookie['QC006'],
            'QC112': new_cookie['QC112'],
            'QC007': new_cookie['QC007'],
            'QC008': new_cookie['QC008'],
            'T00404': new_cookie['T00404'],
            '__uuid': new_cookie['uuid'],
            'play_stream': new_cookie['play_stream'],
            '__dfp': new_cookie['dfp']
            }
            resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
            cookie = resp.cookies
            logger.info('RESP %s - cookie:%s', url, dict(cookie))
    # exit()
    #
    # # tm = 60
    # # url =http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&rfr=&msrc=&vfm=&ve=4493d56139ac57c7c5495a01194d377c&ce=77984f2d6a5a175c2fc6000ec6bcaaab&r=8216139609&vfrm=&u=8580c78be61a04bf404c628440e40a5f&nu=1&pu=&rn=1500698483400&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=a6d0fcb768509a10d01e9a65441d51dd&qdv=1&t=2&tm=60&vid=6eed636cb58c5c822172f51f53a05f91&c1=13&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1
    # # cookie 12
    # tm60rn = str(int(tm15rn) + random.randint(60021, 60030))
    # url = 'http://msg.iqiyi.com/b?%s' % urlencode({
    #     's1': '1',
    #     's2': iqiyi_video_url,
    #     'rfr': '',
    #     'msrc': '',
    #     'vfm': '',
    #     've': config['video_event_id'],  # ?
    #     'ce': config['weid'],  # weid, cookie(QC008)
    #     'r': config_playinfo['tvid'],  # '4175004709',
    #     'vfrm': '',
    #     'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
    #     'nu': nu,
    #     'pu': '',
    #     'rn': tm60rn,  # 和tm=15基准差random.randint(60021, 60030)
    #     'os': 'android',
    #     'ft': 'mp4',
    #     'lrfr': 'DIRECT',
    #     'openid': '',
    #     'pf': '2',
    #     'p': '20',
    #     'p1': '201',
    #     'p2': '1011',
    #     #'__sigC': randomsigC,
    #     #'qdv': '1',
    #     't': '2',
    #     'tm': '60',
    #     'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
    #     'c1': config_playinfo['cid'],
    #     'upderid': '',
    #     'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
    #     'hu': '-1'
    # })
    # logger.info('GET  %s', url)
    # if use_old_cookie:
    #     cookie = {
    #     'QC006': old_cookie['QC006'],
    #     'QC112': old_cookie['QC112'],
    #     'QC007': old_cookie['QC007'],
    #     'QC008': old_cookie['QC008'],
    #     'T00404': old_cookie['T00404'],
    #     '__uuid': old_cookie['uuid'],
    #     'play_stream': old_cookie['play_stream'],
    #     '__dfp': old_cookie['dfp']
    #     }
    #     resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    #     cookie = resp.cookies
    #     logger.info('RESP %s - cookie:%s', url, dict(cookie))
    # else:
    #     cookie = {
    #     'QC006': new_cookie['QC006'],
    #     'QC112': new_cookie['QC112'],
    #     'QC007': new_cookie['QC007'],
    #     'QC008': new_cookie['QC008'],
    #     'T00404': new_cookie['T00404'],
    #     '__uuid': new_cookie['uuid'],
    #     'play_stream': new_cookie['play_stream'],
    #     '__dfp': new_cookie['dfp']
    #     }
    #     resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    #     # soup = BeautifulSoup(resp.content)
    #     cookie = resp.cookies
    #     logger.info('RESP %s - cookie:%s', url, dict(cookie))
    #
    # # tm = 120 第一个
    # # url =
    # # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&rfr=&msrc=&vfm=&ve=4493d56139ac57c7c5495a01194d377c&ce=77984f2d6a5a175c2fc6000ec6bcaaab&r=8216139609&vfrm=&u=8580c78be61a04bf404c628440e40a5f&nu=1&pu=&rn=1500698603439&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=a6d0fcb768509a10d01e9a65441d51dd&qdv=1&t=2&tm=120&vid=6eed636cb58c5c822172f51f53a05f91&c1=13&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1
    # # cookie 13
    # tm120rn1 = str(int(tm60rn) + random.randint(120030, 120050))
    # url = 'http://msg.iqiyi.com/b?%s' % urlencode({
    #     's1': '1',
    #     's2': iqiyi_video_url,
    #     'rfr': '',
    #     'msrc': '',
    #     'vfm': '',
    #     've': config['video_event_id'],  # ?
    #     'ce': config['weid'],  # weid, cookie(QC008)
    #     'r': config_playinfo['tvid'],  # '4175004709',
    #     'vfrm': '',
    #     'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
    #     'nu': nu,
    #     'pu': '',
    #     'rn': tm120rn1,
    #     'os': 'android',
    #     'ft': 'mp4',
    #     'lrfr': 'DIRECT',
    #     'openid': '',
    #     'pf': '2',
    #     'p': '20',
    #     'p1': '201',
    #     'p2': '1011',
    #     #'__sigC': randomsigC,
    #     #'qdv': '1',
    #     't': '2',
    #     'tm': '120',
    #     'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
    #     'c1': config_playinfo['cid'],
    #     'upderid': '',
    #     'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
    #     'hu': '-1'
    # })
    # logger.info('GET  %s', url)
    # if use_old_cookie:
    #     cookie = {
    #     'QC006': old_cookie['QC006'],
    #     'QC112': old_cookie['QC112'],
    #     'QC007': old_cookie['QC007'],
    #     'QC008': old_cookie['QC008'],
    #     'T00404': old_cookie['T00404'],
    #     '__uuid': old_cookie['uuid'],
    #     'play_stream': old_cookie['play_stream'],
    #     '__dfp': old_cookie['dfp']
    #     }
    #     resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    #     cookie = resp.cookies
    #     logger.info('RESP %s - cookie:%s', url, dict(cookie))
    # else:
    #     cookie = {
    #     'QC006': new_cookie['QC006'],
    #     'QC112': new_cookie['QC112'],
    #     'QC007': new_cookie['QC007'],
    #     'QC008': new_cookie['QC008'],
    #     'T00404': new_cookie['T00404'],
    #     '__uuid': new_cookie['uuid'],
    #     'play_stream': new_cookie['play_stream'],
    #     '__dfp': new_cookie['dfp']
    #     }
    #     resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    #     # soup = BeautifulSoup(resp.content)
    #     cookie = resp.cookies
    #     logger.info('RESP %s - cookie:%s', url, dict(cookie))
    #
    # # tm = 120 第二个
    # # ur =
    # # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&rfr=&msrc=&vfm=&ve=4493d56139ac57c7c5495a01194d377c&ce=77984f2d6a5a175c2fc6000ec6bcaaab&r=8216139609&vfrm=&u=8580c78be61a04bf404c628440e40a5f&nu=1&pu=&rn=1500698723478&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=a6d0fcb768509a10d01e9a65441d51dd&qdv=1&t=2&tm=120&vid=6eed636cb58c5c822172f51f53a05f91&c1=13&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1
    # # cookie 14
    # tm120rn2 = str(int(tm120rn1) + random.randint(120030, 120050))
    # url = 'http://msg.iqiyi.com/b?%s' % urlencode({
    #     's1': '1',
    #     's2': iqiyi_video_url,
    #     'rfr': '',
    #     'msrc': '',
    #     'vfm': '',
    #     've': config['video_event_id'],  # ?
    #     'ce': config['weid'],  # weid, cookie(QC008)
    #     'r': config_playinfo['tvid'],  # '4175004709',
    #     'vfrm': '',
    #     'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
    #     'nu': nu,
    #     'pu': '',
    #     'rn': tm120rn2,
    #     'os': 'android',
    #     'ft': 'mp4',
    #     'lrfr': 'DIRECT',
    #     'openid': '',
    #     'pf': '2',
    #     'p': '20',
    #     'p1': '201',
    #     'p2': '1011',
    #     #'__sigC': randomsigC,
    #     #'qdv': '1',
    #     't': '2',
    #     'tm': '120',
    #     'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
    #     'c1': config_playinfo['cid'],
    #     'upderid': '',
    #     'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
    #     'hu': '-1'
    # })
    # logger.info('GET  %s', url)
    # if use_old_cookie:
    #     cookie = {
    #     'QC006': old_cookie['QC006'],
    #     'QC112': old_cookie['QC112'],
    #     'QC007': old_cookie['QC007'],
    #     'QC008': old_cookie['QC008'],
    #     'T00404': old_cookie['T00404'],
    #     '__uuid': old_cookie['uuid'],
    #     'play_stream': old_cookie['play_stream'],
    #     '__dfp': old_cookie['dfp']
    #     }
    #     resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    # # soup = BeautifulSoup(resp.content)
    #     cookie = resp.cookies
    #     logger.info('RESP %s - cookie:%s', url, dict(cookie))
    # else:
    #     cookie = {
    #     'QC006': new_cookie['QC006'],
    #     'QC112': new_cookie['QC112'],
    #     'QC007': new_cookie['QC007'],
    #     'QC008': new_cookie['QC008'],
    #     'T00404': new_cookie['T00404'],
    #     '__uuid': new_cookie['uuid'],
    #     'play_stream': new_cookie['play_stream'],
    #     '__dfp': new_cookie['dfp']
    #     }
    #     resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
    #     # soup = BeautifulSoup(resp.content)
    #     cookie = resp.cookies
    #     logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # http://nl.rcd.iqiyi.com/apis/urc/setrc?videoPlayTime=0&tvId=4175004709&videoName=Jolla%E5%8F%91%E5%B8%83Sailfish+2.0%E5%B9%B6%E5%B8%A6%E6%9D%A5%E5%85%A8%E6%96%B0UI&terminalId=31&agent_type=13&ad=1500169624&ckuid=e3a08c4942b6550f8b6600e1706aa24c&_=1500169623796&callback=Zepto1500169415742
    # '_': ''.join(random.choice(string.digits) for _ in range(13)),
    # 'ad': str(int((config['start_time'] + int(config_playinfo['duration']) + 32000) / 1000)),
    # 'callback': 'Zepto%s' % (''.join(random.choice(string.digits) for _ in range(13))),
    # 'videoPlayTime': config_playinfo['duration'],
    # videoPlayTime不是一个定值，是通过这个链接的"_"的值减去点击播放的链接"_"的值四舍五入得到
    # '_'的值在tm=120(第二个的基础上面)随机（88000,90100）
    # cookie 15
    print('videoPlayTime406')
    play_d = str(int(oldbasetime) + random.randint(406000, 406499))
    videoPlayTime406 = config_playinfo['duration']
    Zepto1 = str(int(Zepto) + 1)
    ad = str(int(int(play_d) / 1000))
    url = 'http://nl.rcd.iqiyi.com/apis/urc/setrc?%s' % urlencode({
        'videoPlayTime': videoPlayTime406,
        'tvId': config_playinfo['tvid'],  # '4175004709',
        # 'Jolla发布Sailfish 2.0并带来全新UI',
        'videoName': config_playinfo['shortTitle'],
        'terminalId': '31',
        'agent_type': '13',
        'ad': ad,
        'ckuid': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
        '_': play_d,
        'callback': 'Zepto%s' % Zepto1,
    })
    if use_old_cookie:
        cookie = {
        'QC006': old_cookie['QC006'],
        'QC112': old_cookie['QC112'],
        'QC007': old_cookie['QC007'],
        'QC008': old_cookie['QC008'],
        'T00404': old_cookie['T00404'],
        '__uuid': old_cookie['uuid'],
        'play_stream': old_cookie['play_stream'],
        '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
        'QC006': new_cookie['QC006'],
        'QC112': new_cookie['QC112'],
        'QC007': new_cookie['QC007'],
        'QC008': new_cookie['QC008'],
        'T00404': new_cookie['T00404'],
        '__uuid': new_cookie['uuid'],
        'play_stream': new_cookie['play_stream'],
        '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # last
    # http://m.iqiyi.com/api/cloud/code?_tv_id_=8286483209&vfm=&_=1500424710554&callback=Zepto1500424417239
    # 'callback': 'Zepto%s' % (''.join(random.choice(string.digits) for _ in range(13)))
    # '_': ''.join(random.choice(string.digits) for _ in range(13)),
    # cookie 16
    temp = int(oldbasetime) + random.randint(406000,
                                            406499) + random.randint(1, 99)
    Zeptol = str(int(Zepto) + 5)
    url = 'http://m.iqiyi.com/api/cloud/code?%s' % urlencode({
        '_tv_id_': config_playinfo['tvid'],
        'vfm': '',
        '_': str(temp - random.randint(1, 9)),
        'callback': 'Zepto%s' % Zeptol
    })
    if use_old_cookie:
        cookie = {
        'QC006': old_cookie['QC006'],
        'QC112': old_cookie['QC112'],
        'QC007': old_cookie['QC007'],
        'QC008': old_cookie['QC008'],
        'T00404': old_cookie['T00404'],
        '__uuid': old_cookie['uuid'],
        'Hm_lvt_5df871ab99f94347b23ca224fc7d013f': old_cookie['Hm_lvt_5df871ab99f94347b23ca224fc7d013f'],
        'Hm_lpvt_5df871ab99f94347b23ca224fc7d013f': old_cookie['Hm_lpvt_5df871ab99f94347b23ca224fc7d013f'],
        'play_stream': old_cookie['play_stream'],
        '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
        'QC006': new_cookie['QC006'],
        'QC112': new_cookie['QC112'],
        'QC007': new_cookie['QC007'],
        'QC008': new_cookie['QC008'],
        'T00404': new_cookie['T00404'],
        '__uuid': new_cookie['uuid'],
        'Hm_lvt_5df871ab99f94347b23ca224fc7d013f': new_cookie['Hm_lvt_5df871ab99f94347b23ca224fc7d013f'],
        'Hm_lpvt_5df871ab99f94347b23ca224fc7d013f': new_cookie['Hm_lpvt_5df871ab99f94347b23ca224fc7d013f'],
        'play_stream': new_cookie['play_stream'],
        '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # videoPlayTime为0
    # cookie 17
    print('videoPlayTime0')
    play_d1 = str(int(oldbasetime) + random.randint(406000, 406499) + random.randint(1, 99))
    Zepto2 = str(int(Zepto1) + 3)
    ad1 = str(int(int(play_d1) / 1000))
    url = 'http://nl.rcd.iqiyi.com/apis/urc/setrc?%s' % urlencode({
        'videoPlayTime': '0',
        'tvId': config_playinfo['tvid'],  # '4175004709',
        # 'Jolla发布Sailfish 2.0并带来全新UI',
        'videoName': config_playinfo['shortTitle'],
        'terminalId': '31',
        'agent_type': '13',
        'ad': ad1,
        'ckuid': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
        '_': play_d1,
        'callback': 'Zepto%s' % Zepto1,
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
        'QC006': old_cookie['QC006'],
        'QC112': old_cookie['QC112'],
        'QC007': old_cookie['QC007'],
        'QC008': old_cookie['QC008'],
        'T00404': old_cookie['T00404'],
        '__uuid': old_cookie['uuid'],
        'play_stream': old_cookie['play_stream'],
        '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
        'QC006': new_cookie['QC006'],
        'QC112': new_cookie['QC112'],
        'QC007': new_cookie['QC007'],
        'QC008': new_cookie['QC008'],
        'T00404': new_cookie['T00404'],
        '__uuid': new_cookie['uuid'],
        'play_stream': new_cookie['play_stream'],
        '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))

    # 在tm为90之前还有一个没有tm的请求
    # cookie 18
    tm90rn = str(int(tm120rn2) + random.randint(120030, 120050))
    rn = str(int(tm90rn) + 6)
    url = 'http://msg.iqiyi.com/b?%s' % urlencode({
        "s1": "1",
        "s2": iqiyi_video_url,
        "rfr": "",
        "msrc": "",
        "vfm": "",
        "ve": config['video_event_id'],
        "ce": config['weid'],
        "r": "8216139609",
        "vfrm": "",
        "u": config['user_id'],
        "nu": "1",
        "pu": "",
        "rn": "1501162154633",
        "os": "android",
        "ft": "mp4",
        "lrfr": "DIRECT",
        "openid": "",
        "pf": "2",
        "p": "20",
        "p1": "201",
        "p2": "1011",
        "t": "13",
        "vid": config_playinfo['vid'],
        "c1": config_playinfo['cid'],
        "upderid": "",
        "dfp": "e050d573b52efb4fd8acb5118c794489f10cc133cf7e6eecfaad465d3bf97d264b",
        "hu": "-1"
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
            'QC006': old_cookie['QC006'],
            'QC112': old_cookie['QC112'],
            'QC007': old_cookie['QC007'],
            'QC008': old_cookie['QC008'],
            'T00404': old_cookie['T00404'],
            '__uuid': old_cookie['uuid'],
            'play_stream': old_cookie['play_stream'],
            '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
        'QC006': new_cookie['QC006'],
        'QC112': new_cookie['QC112'],
        'QC007': new_cookie['QC007'],
        'QC008': new_cookie['QC008'],
        'T00404': new_cookie['T00404'],
        '__uuid': new_cookie['uuid'],
        'play_stream': new_cookie['play_stream'],
        '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # tm为90这部分在videoPlayTime这个请求的后面
    # tm = 90
    # url =
    # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rtnxsab9.html&rfr=&msrc=&vfm=&ve=aabf418cf8a39abce0e907a1869e9433&ce=278200f63d6a60b8cdfad813950c2b90&r=8216139609&vfrm=&u=38ce3b3a80cc25e533a3f0a80914e995&nu=1&pu=&rn=1500690050407&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=6ba3514c24b845002132d837a0e26332&qdv=1&t=2&tm=90&vid=6eed636cb58c5c822172f51f53a05f91&c1=13&upderid=&dfp=e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f&hu=-1
    # cookie 19
    tm90 = str(int((int(play_d) - int(tm120rn2)) / 1000))
    url = 'http://msg.iqiyi.com/b?%s' % urlencode({
        's1': '1',
        's2': iqiyi_video_url,
        'rfr': '',
        'msrc': '',
        'vfm': '',
        've': config['video_event_id'],  # ?
        'ce': config['weid'],  # weid, cookie(QC008)
        'r': config_playinfo['tvid'],  # '4175004709',
        'vfrm': '',
        'u': config['user_id'],  # 'e3a08c4942b6550f8b6600e1706aa24c',
        'nu': nu,
        'pu': '',
        'rn': tm90rn,
        'os': 'android',
        'ft': 'mp4',
        'lrfr': 'DIRECT',
        'openid': '',
        'pf': '2',
        'p': '20',
        'p1': '201',
        'p2': '1011',
        #'__sigC': randomsigC,
        #'qdv': '1',
        't': '2',
        'tm': tm90,
        'vid': config_playinfo['vid'],  # 'a462923b370a4a880aef5d74bcef2079',
        'c1': '60',
        'upderid': '',
        'dfp': 'e0da28f1d7d4fa4725a86bbdc1f254e52f79bea0b6798620c2fbff98cd5609364f',  # ?
        'hu': '-1'
    })
    logger.info('GET  %s', url)
    if use_old_cookie:
        cookie = {
        'QC006': old_cookie['QC006'],
        'QC112': old_cookie['QC112'],
        'QC007': old_cookie['QC007'],
        'QC008': old_cookie['QC008'],
        'T00404': old_cookie['T00404'],
        '__uuid': old_cookie['uuid'],
        'play_stream': old_cookie['play_stream'],
        '__dfp': old_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))
    else:
        cookie = {
        'QC006': new_cookie['QC006'],
        'QC112': new_cookie['QC112'],
        'QC007': new_cookie['QC007'],
        'QC008': new_cookie['QC008'],
        'T00404': new_cookie['T00404'],
        '__uuid': new_cookie['uuid'],
        'play_stream': new_cookie['play_stream'],
        '__dfp': new_cookie['dfp']
        }
        resp = requests.get(url, headers=headers(base_headers(url, ua, ref=iqiyi_video_url)), cookies=cookie, timeout=10)
        # soup = BeautifulSoup(resp.content)
        cookie = resp.cookies
        logger.info('RESP %s - cookie:%s', url, dict(cookie))


    # url = 'http://m.iqiyi.com/api/cloud/code?%s' % urlencode({
    #     '_tv_id_': '331529200',
    #     '_': ''.join(random.choice(string.digits) for _ in range(13)),
    #     'callback': 'Zepto%s' % (''.join(random.choice(string.digits) for _ in range(13)))
    # })
    logger.debug('trigger log')
    # video_name
    url = 'http://f.valuecome.com/ip?%s' % video_name
    logger.info(url)
    resp = requests.get(url, timeout=20)
    # logger.debug('sleep')
    # time.sleep(random.choice(list(range(10, 30))))
    # http://msg.iqiyi.com/b?s1=1&s2=http%3A%2F%2Fm.iqiyi.com%2Fw_19rrtl7yh5.html&rfr=&msrc=&vfm=&ve=faf0c728031136fd62877e46b842271f&ce=134a8209ff557db9c226f757c0cbeaaf&r=4175004709&vfrm=&u=e3a08c4942b6550f8b6600e1706aa24c&nu=1&pu=&rn=1500169623750&os=android&ft=mp4&lrfr=DIRECT&openid=&pf=2&p=20&p1=201&p2=1011&__sigC=deb107ae60d799d1be8f2f8591b1ca12&qdv=1&t=2&tm=37&vid=a462923b370a4a880aef5d74bcef2079&c1=30&upderid=&dfp=e11bf77bb301f748a39fe1b2c53bb4d397e978b53cc45986c66a62dc1327a21df9&hu=-1
    logger.info('cookie operate')
    if not use_old_cookie:
        #本次使用新cookie，则返回本次形成的新cookie
        logger.debug('return new_cookie')
        # 整个运行过程中使用到的cookie
        return_cookie = new_cookie
        return_cookie['nu'] = '0' #老用户nu为0
    else:
        return_cookie = old_cookie

    # video_name = task_parameters['videoid']
    return video_name, use_old_cookie, return_cookie




if __name__ == '__main__':
    logger.info('read network info')
    with open('config.json', 'r', encoding='utf-8') as f:
        fconfig = json.load(f)
    net_wait = fconfig.get('net_wait', 5)
    net_name = fconfig['net_name']
    net_pwd = fconfig['net_pwd']
    this_net_wait = net_wait

    # ip去重
    if os.path.isfile('ippool.json'):
        print('有IP记录')
        with open('ippool.json', 'r', encoding='utf-8') as f:
            ippool = json.load(f)
    else:
        ippool = []
    while True:
        time.sleep(0)
        logger.info('connect Internet')
        connected = False
        while not connected:
            try:
                subprocess.Popen(["rasphone", "-h", "宽带连接"]).communicate()
            except BaseException as e:
                sys.stderr.write('%s\n' % e)

            print('sleep %s' % this_net_wait)
            time.sleep(this_net_wait)

            try:
                subprocess.Popen(["rasdial", "宽带连接", net_name, net_pwd]).communicate()
            except BaseException as e:
                sys.stderr.write('%s\n' % e)

            try:
                print('connet baidu confirm')
                resp = requests.get('https://www.baidu.com/', timeout=10)
            except requests.exceptions.Timeout as e:
                sys.stderr.write(str(e))
                sys.stderr.write('\n')
                print('sleep 10')
                time.sleep(10)
                continue
            else:
                print('get ip')
                try:
                    resp = requests.get('http://f.valuecome.com/ip', timeout=10)
                except requests.exceptions.Timeout as e:
                    sys.stderr.write(str(e))
                    sys.stderr.write('\n')
                    print('sleep 10')
                    time.sleep(10)
                    continue
                else:
                    if resp.status_code < 400:
                        print('connected')
                        ip = resp.text.strip()
                        if ip in ippool:
                            print('IP: %s 已经被使用，重新获取' % ip)
                            continue
                        else:
                            ippool.append(ip)
                            connected = True
                            break
        print('宽带链接成功')
        # time.sleep(5)
        print('IP存档')
        with open('ippool.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(ippool))

        # tasks_list = [{"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "22", "tvid": "8218061909", "shortTitle": "\\u6CF0\\u56FD\\u5927\\u8138\\u7F51\\u7EA2\\u59B9\\u5986\\u5BB9\\u6559\\u7A0B\\u6CB3\\u5317\\u65B9\\u8A00\\u914D\\u97F3\\u89E3\\u8BF4", "aid": "8218061909", "vid": "aa956570398a04cb7cee136036736aaa", "duration": "432", "qipuId": "8218061909"}, "ip": "", "uv_ratio": 0.7, "videoid": "fangyanpeiyinjieshuo", "url": "http://m.iqiyi.com/w_19rtnwd5x5.html#vfrm=8-8-0-1", "old_cookie": ""}, {"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "13", "tvid": "8177052609", "shortTitle": "\\u7B80\\u5355\\u5FEB\\u901F\\u6253\\u9020\\u7EA6\\u4F1A\\u5986\\u5BB9\\u77AC\\u95F4\\u63D0\\u5347\\u6843\\u82B1\\u6C14", "aid": "8177052609", "vid": "bc5d62e5444f24fa58704429b3d78ac3", "duration": "1498", "qipuId": "8177052609"}, "ip": "", "uv_ratio": 0.7, "videoid": "jiantishengtaohuaqi", "url": "http://m.iqiyi.com/w_19rtrmi20d.html#vfrm=8-8-0-1", "old_cookie": ""}, {"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "13", "tvid": "8174755909", "shortTitle": "\\u5723\\u7F57\\u5170YSL\\u4EBA\\u9C7C\\u59EC\\u8272112\\uFF03\\u548C\\u66FF\\u6362\\u8272111\\uFF03\\u53E3\\u7EA2\\u8BD5\\u8272\\u5BF9\\u6BD4", "aid": "8174755909", "vid": "e76e64ca3c088ca226d0f033447ba352", "duration": "355", "qipuId": "8174755909"}, "ip": "", "uv_ratio": 0.7, "videoid": "kouhongshiseduibi", "url": "http://m.iqiyi.com/w_19rtrnpkix.html#vfrm=8-8-0-1", "old_cookie": ""}, {"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "13", "tvid": "8171062409", "shortTitle": "\\u7F8E\\u5986\\u6D4B\\u8BC4\\u4E4B\\u5982\\u4F55\\u8BA9\\u776B\\u6BDB\\u53C8\\u5377\\u53C8\\u4FCF", "aid": "8171062409", "vid": "7fbfea24698ea72ed8014b135fd7f9b2", "duration": "292", "qipuId": "8171062409"}, "ip": "", "uv_ratio": 0.7, "videoid": "jiemaoyoujuanyouqiao", "url": "http://m.iqiyi.com/w_19rtrqj7qd.html#vfrm=8-8-0-1", "old_cookie": ""}, {"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "13", "tvid": "8170364609", "shortTitle": "\\u4E0D\\u7528\\u776B\\u6BDB\\u5939\\u4E5F\\u53EF\\u4EE5\\u8BA9\\u773C\\u776B\\u6BDB\\u66F4\\u5377\\u7FD8\\u7684\\u65B9\\u6CD5", "aid": "8170364609", "vid": "082a178827e96196178a019a4f0ca578", "duration": "442", "qipuId": "8170364609"}, "ip": "", "uv_ratio": 0.7, "videoid": "gengjuanqiaodefangfa", "url": "http://m.iqiyi.com/w_19rts199kd.html#vfrm=8-8-0-1", "old_cookie": ""}, {"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "13", "tvid": "8162911309", "shortTitle": "\\u5FEB\\u901F\\u5B8C\\u6210\\u9738\\u6C14\\u6B27\\u7F8E\\u5986\\u5BB9", "aid": "8162911309", "vid": "b8aaf693ea3f56c1ca8192d181a7764d", "duration": "541", "qipuId": "8162911309"}, "ip": "", "uv_ratio": 0.7, "videoid": "baqioumeizhuangrong", "url": "http://m.iqiyi.com/w_19rts5ncbl.html#vfrm=8-8-0-1", "old_cookie": ""}, {"process_number": 1, "click_ad_ratio": 0.002, "video_info": {"cid": "13", "tvid": "8135522309", "shortTitle": "\\u7F8E\\u5986\\u6D4B\\u8BC4\\u4E4B\\u590F\\u65E5\\u5FC5\\u5907\\u5B9D\\u9732\\u9732\\u9632\\u868A\\u6263", "aid": "8135522309", "vid": "c2eb1acf8796bde827f0c069a064727c", "duration": "233", "qipuId": "8135522309"}, "ip": "", "uv_ratio": 0.7, "videoid": "baolulufangwenkou", "url": "http://m.iqiyi.com/w_19rtrhunhl.html#vfrm=8-8-0-1", "old_cookie": ""}]
        # wunv_tasks_list = [{"ip": "", "old_cookie": "", "videoid": "bieshabiebieniao", "url": "http://m.iqiyi.com/w_19rtsb9eod.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u4F60\\u7231\\u618B\\u5C3F\\u5417 \\u957F\\u671F\\u618B\\u5C3F\\u4F1A\\u8BF1\\u53D1\\u8180\\u80F1\\u764C \\u618B\\u5565\\u522B\\u618B\\u5C3F", "cid": "27", "duration": "85", "aid": "8489040609", "qipuId": "8489040609", "vid": "8daf36e7022cbb16958f06d560be5f52", "tvid": "8489040609"}, "uv_ratio": 0.7}, {"ip": "", "old_cookie": "", "videoid": "xinlemelaotie", "url": "http://m.iqiyi.com/w_19rtsb9jcd.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u7537\\u4EBA\\u6700\\u6015\\u7684\\u4E94\\u4E2A\\u5B57 \\u624E\\u5FC3\\u4E86\\u4E48\\u8001\\u94C1", "cid": "27", "duration": "136", "aid": "8489030209", "qipuId": "8489030209", "vid": "5a1ba4c2b11125518a3371790458fda2", "tvid": "8489030209"}, "uv_ratio": 0.7}, {"ip": "", "old_cookie": "", "videoid": "nizhidaoma", "url": "http://m.iqiyi.com/w_19rtv4ef1h.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u6D17\\u9E33\\u9E2F\\u6D74\\u4F1A\\u5BFC\\u81F4\\u7537\\u6027\\u6027\\u80FD\\u529B\\u4F4E\\u4E0B\\u4EE5\\u53CA\\u4E0D\\u5B55\\u4E0D\\u80B2 \\u4F60\\u77E5\\u9053\\u5417", "cid": "32", "duration": "91", "aid": "8386270409", "qipuId": "8386270409", "vid": "076ef448d0e47c70320f37ddf51001eb", "tvid": "8386270409"}, "uv_ratio": 0.7}, {"ip": "", "old_cookie": "", "videoid": "kuangyinjiuzhihou", "url": "http://m.iqiyi.com/w_19rtun4ilx.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u7EC8\\u4E8E\\u548C\\u5973\\u795E\\u5F00\\u623F\\u4E86 \\u7D27\\u5F20\\u5730\\u72C2\\u996E\\u9152\\u4E4B\\u540E", "cid": "22", "duration": "84", "aid": "8346866009", "qipuId": "8346866009", "vid": "45d029683163b070fc8b5f6173347fb0", "tvid": "8346866009"}, "uv_ratio": 0.7}, {"ip": "", "old_cookie": "", "videoid": "xiongwaikeyisheng", "url": "http://m.iqiyi.com/w_19rtusrvm1.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u4E24\\u8FB9\\u80F8\\u4E0D\\u4E00\\u6837\\u5927\\u600E\\u4E48\\u529E \\u5927\\u80F8\\u7F8E\\u5973PK\\u80F8\\u5916\\u79D1\\u533B\\u751F", "cid": "22", "duration": "87", "aid": "8337382309", "qipuId": "8337382309", "vid": "4f6aaff5fcf5f6d2ac1d6a04604e4983", "tvid": "8337382309"}, "uv_ratio": 0.7}, {"ip": "", "old_cookie": "", "videoid": "lebushihaoshi", "url": "http://m.iqiyi.com/w_19rtpbvoq1.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u7070\\u973E\\u53D8\\u80BA\\u764C\\u7684\\u4E34\\u5E8A\\u75C7\\u72B6\\u6709\\u8FD9\\u4E9B \\u65E0\\u7F18\\u65E0\\u6545\\u7626\\u4E86\\u4E0D\\u662F\\u597D\\u4E8B", "cid": "21", "duration": "96", "aid": "8266738309", "qipuId": "8266738309", "vid": "43bb1b6f1c267b949137bb195048fa71", "tvid": "8266738309"}, "uv_ratio": 0.7}, {"ip": "", "old_cookie": "", "videoid": "dezhengquezishi", "url": "http://m.iqiyi.com/w_19rtpcm8kh.html#vfrm=8-8-0-1", "process_number": 1, "click_ad_ratio": 0.002, "video_info": {"shortTitle": "\\u767D\\u9152\\u548C\\u5564\\u9152\\u4E3A\\u4EC0\\u4E48\\u4E0D\\u80FD\\u63BA\\u7740\\u559D \\u590F\\u5B63\\u64B8\\u4E32\\u996E\\u9152\\u7684\\u6B63\\u786E\\u59FF\\u52BF", "cid": "21", "duration": "113", "aid": "8265831309", "qipuId": "8265831309", "vid": "fc746d950c8f3af04584ffed795584b8", "tvid": "8265831309"}, "uv_ratio": 0.7}]
        # wanmei_tasks_list = [{"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u4E0D\\u82B1\\u94B1\\u7A7F\\u5230\\u590F\\u65E5\\u6700\\u65F6\\u5C1A\\u7684\\u4EBA\\u5B57\\u62D6", "cid": "21", "qipuId": "746672800", "vid": "d38243b39120c4ce621c63b61ca1bb3a", "duration": "66", "aid": "746672800", "tvid": "746672800"}, "url": "http://m.iqiyi.com/v_19rr8kv00c.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "jiuwufanxin", "process_number": 1}, {"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u5356\\u8138\\u4E3B\\u64AD\\u53D8\\u5403\\u64AD", "cid": "21", "qipuId": "746466800", "vid": "1a4e315eee7d56d3a91272e9bf62e3cb", "duration": "125", "aid": "746466800", "tvid": "746466800"}, "url": "http://m.iqiyi.com/v_19rr8c32cc.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "shenghuo", "process_number": 1}, {"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u65E5\\u672C\\u77E5\\u540D\\u6A21\\u7279\\u6559\\u4F60\\u7A7F\\u8863", "cid": "21", "qipuId": "745771000", "vid": "de417ab1f334ef0f65170b7a02c1b5e2", "duration": "76", "aid": "745771000", "tvid": "745771000"}, "url": "http://m.iqiyi.com/v_19rr8blo5w.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "ribenzhiming", "process_number": 1}, {"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u9762\\u819C\\u7684\\u6B63\\u786E\\u4F7F\\u7528\\u65B9\\u6CD5\\uFF01", "cid": "21", "qipuId": "744874500", "vid": "37bf5af7a2d9c39635351e8624147288", "duration": "77", "aid": "744874500", "tvid": "744874500"}, "url": "http://m.iqiyi.com/v_19rr8caeso.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "mianmodezheng", "process_number": 1}, {"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u95EA\\u4EAE\\u8FF7\\u4EBA\\u7684\\u97E9\\u5F0F\\u5986\\u5BB9", "cid": "21", "qipuId": "743903600", "vid": "b901cbf78cd7dfff65c685baac9ceaf3", "duration": "60", "aid": "743903600", "tvid": "743903600"}, "url": "http://m.iqiyi.com/v_19rr8czlzw.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "shanliangmiren", "process_number": 1}, {"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u65E5\\u672C\\u7F8E\\u5986\\u535A\\u4E3B\\u4EB2\\u81EA\\u4F20\\u6388\\u79D8\\u7C4D", "cid": "21", "qipuId": "741241000", "vid": "94196fdad8b4d1a650f5bfd1d2c59b9d", "duration": "54", "aid": "741241000", "tvid": "741241000"}, "url": "http://m.iqiyi.com/v_19rr8f75qc.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "ribenmeizhuang", "process_number": 1}, {"click_ad_ratio": 0.002, "ip": "", "video_info": {"shortTitle": "\\u7ED9\\u4EB2\\u7231\\u7684TA\\u505A\\u725B\\u4E94\\u82B1\\u70E4\\u8089\\u996D", "cid": "21", "qipuId": "741243800", "vid": "fa38a6dd8eb83934be2bf77a3b0a3059", "duration": "43", "aid": "741243800", "tvid": "741243800"}, "url": "http://m.iqiyi.com/v_19rr8f76r8.html#vfrm=8-8-0-1", "uv_ratio": 0.2, "old_cookie": "", "videoid": "zuitianmide", "process_number": 1}]
        # wanmei_tasks_list = [{"uv_ratio": 0.2, "video_info": {"vid": "d38243b39120c4ce621c63b61ca1bb3a", "shortTitle": "\\u4E0D\\u82B1\\u94B1\\u7A7F\\u5230\\u590F\\u65E5\\u6700\\u65F6\\u5C1A\\u7684\\u4EBA\\u5B57\\u62D6", "qipuId": "746672800", "tvid": "746672800", "duration": "66", "aid": "746672800", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8kv00c.html#vfrm=8-8-0-1", "videoid": "v_19rr8kv00c", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}, {"uv_ratio": 0.2, "video_info": {"vid": "1a4e315eee7d56d3a91272e9bf62e3cb", "shortTitle": "\\u5356\\u8138\\u4E3B\\u64AD\\u53D8\\u5403\\u64AD", "qipuId": "746466800", "tvid": "746466800", "duration": "125", "aid": "746466800", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8c32cc.html#vfrm=8-8-0-1", "videoid": "v_19rr8c32cc", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}, {"uv_ratio": 0.2, "video_info": {"vid": "de417ab1f334ef0f65170b7a02c1b5e2", "shortTitle": "\\u65E5\\u672C\\u77E5\\u540D\\u6A21\\u7279\\u6559\\u4F60\\u7A7F\\u8863", "qipuId": "745771000", "tvid": "745771000", "duration": "76", "aid": "745771000", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8blo5w.html#vfrm=8-8-0-1", "videoid": "v_19rr8blo5w", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}, {"uv_ratio": 0.2, "video_info": {"vid": "37bf5af7a2d9c39635351e8624147288", "shortTitle": "\\u9762\\u819C\\u7684\\u6B63\\u786E\\u4F7F\\u7528\\u65B9\\u6CD5\\uFF01", "qipuId": "744874500", "tvid": "744874500", "duration": "77", "aid": "744874500", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8caeso.html#vfrm=8-8-0-1", "videoid": "v_19rr8caeso", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}, {"uv_ratio": 0.2, "video_info": {"vid": "b901cbf78cd7dfff65c685baac9ceaf3", "shortTitle": "\\u95EA\\u4EAE\\u8FF7\\u4EBA\\u7684\\u97E9\\u5F0F\\u5986\\u5BB9", "qipuId": "743903600", "tvid": "743903600", "duration": "60", "aid": "743903600", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8czlzw.html#vfrm=8-8-0-1", "videoid": "v_19rr8czlzw", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}, {"uv_ratio": 0.2, "video_info": {"vid": "94196fdad8b4d1a650f5bfd1d2c59b9d", "shortTitle": "\\u65E5\\u672C\\u7F8E\\u5986\\u535A\\u4E3B\\u4EB2\\u81EA\\u4F20\\u6388\\u79D8\\u7C4D", "qipuId": "741241000", "tvid": "741241000", "duration": "54", "aid": "741241000", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8f75qc.html#vfrm=8-8-0-1", "videoid": "v_19rr8f75qc", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}, {"uv_ratio": 0.2, "video_info": {"vid": "fa38a6dd8eb83934be2bf77a3b0a3059", "shortTitle": "\\u7ED9\\u4EB2\\u7231\\u7684TA\\u505A\\u725B\\u4E94\\u82B1\\u70E4\\u8089\\u996D", "qipuId": "741243800", "tvid": "741243800", "duration": "43", "aid": "741243800", "cid": "21"}, "ip": "", "url": "http://m.iqiyi.com/v_19rr8f76r8.html#vfrm=8-8-0-1", "videoid": "v_19rr8f76r8", "old_cookie": "", "click_ad_ratio": 0.002, "process_number": 1}]
        # iqiyi_new_4 = [{"process_number": 1, "uv_ratio": 0.2, "videoid": "v_19rr6yfioc", "url": "http://m.iqiyi.com/v_19rr6yfioc.html", "old_cookie": "", "video_info": {"vid": "f9436e2cd6b2e4ec26172697da486c04", "qipuId": "694669600", "cid": "12", "duration": "392", "aid": "207638801", "tvid": "694669600", "shortTitle": "\\u4E00\\u665A\\u51E0\\u6B21\\u611F\\u89C9\\u8EAB\\u4F53\\u88AB\\u638F\\u7A7A"}, "ip": "", "click_ad_ratio": 0.002}, {"process_number": 1, "uv_ratio": 0.2, "videoid": "v_19rr7epzlo", "url": "http://m.iqiyi.com/v_19rr7epzlo.html", "old_cookie": "", "video_info": {"vid": "6184e10e9d68200d91a6ace9a172c0d5", "qipuId": "684285200", "cid": "12", "duration": "207", "aid": "684285200", "tvid": "684285200", "shortTitle": "diy\\u51E0\\u6B3E\\u597D\\u559D\\u53C8\\u597D\\u770B\\u7684\\u6C14\\u6CE1\\u6C34"}, "ip": "", "click_ad_ratio": 0.002}, {"process_number": 1, "uv_ratio": 0.2, "videoid": "v_19rr7eq0gw", "url": "http://m.iqiyi.com/v_19rr7eq0gw.html", "old_cookie": "", "video_info": {"vid": "80197b3ecf0440f3a539936d877ace10", "qipuId": "684284300", "cid": "12", "duration": "189", "aid": "684284300", "tvid": "684284300", "shortTitle": "\\u6700\\u7F8E\\u4E0D\\u8FC7\\u679C\\u852C\\u6C99\\u62C9\\u9178\\u5976\\u9171\\u7684\\u9082\\u9005"}, "ip": "", "click_ad_ratio": 0.002}, {"process_number": 1, "uv_ratio": 0.2, "videoid": "v_19rr7eq0ck", "url": "http://m.iqiyi.com/v_19rr7eq0ck.html", "old_cookie": "", "video_info": {"vid": "87b0217fb115a2391d7ea542f049dd79", "qipuId": "684284200", "cid": "12", "duration": "220", "aid": "684284200", "tvid": "684284200", "shortTitle": "\\u708E\\u708E\\u590F\\u65E5\\uFF0C\\u4F60\\u9700\\u8981\\u4E00\\u5757\\u513F\\u897F\\u74DC"}, "ip": "", "click_ad_ratio": 0.002}]
        # iqiyi_new_4 = [{"process_number": 1, "uv_ratio": 0.1, "videoid": "v_19rr6yfioc", "url": "http://m.iqiyi.com/v_19rr6yfioc.html", "old_cookie": "", "video_info": {"vid": "f9436e2cd6b2e4ec26172697da486c04", "qipuId": "694669600", "cid": "12", "duration": "392", "aid": "207638801", "tvid": "694669600", "shortTitle": "\\u4E00\\u665A\\u51E0\\u6B21\\u611F\\u89C9\\u8EAB\\u4F53\\u88AB\\u638F\\u7A7A"}, "ip": "", "click_ad_ratio": 0.002}, {"process_number": 1, "uv_ratio": 0.1, "videoid": "v_19rr7epzlo", "url": "http://m.iqiyi.com/v_19rr7epzlo.html", "old_cookie": "", "video_info": {"vid": "6184e10e9d68200d91a6ace9a172c0d5", "qipuId": "684285200", "cid": "12", "duration": "207", "aid": "684285200", "tvid": "684285200", "shortTitle": "diy\\u51E0\\u6B3E\\u597D\\u559D\\u53C8\\u597D\\u770B\\u7684\\u6C14\\u6CE1\\u6C34"}, "ip": "", "click_ad_ratio": 0.002}, {"process_number": 1, "uv_ratio": 0.1, "videoid": "v_19rr7eq0gw", "url": "http://m.iqiyi.com/v_19rr7eq0gw.html", "old_cookie": "", "video_info": {"vid": "80197b3ecf0440f3a539936d877ace10", "qipuId": "684284300", "cid": "12", "duration": "189", "aid": "684284300", "tvid": "684284300", "shortTitle": "\\u6700\\u7F8E\\u4E0D\\u8FC7\\u679C\\u852C\\u6C99\\u62C9\\u9178\\u5976\\u9171\\u7684\\u9082\\u9005"}, "ip": "", "click_ad_ratio": 0.002}, {"process_number": 1, "uv_ratio": 0.1, "videoid": "v_19rr7eq0ck", "url": "http://m.iqiyi.com/v_19rr7eq0ck.html", "old_cookie": "", "video_info": {"vid": "87b0217fb115a2391d7ea542f049dd79", "qipuId": "684284200", "cid": "12", "duration": "220", "aid": "684284200", "tvid": "684284200", "shortTitle": "\\u708E\\u708E\\u590F\\u65E5\\uFF0C\\u4F60\\u9700\\u8981\\u4E00\\u5757\\u513F\\u897F\\u74DC"}, "ip": "", "click_ad_ratio": 0.002}]

        iqiyi_new_4 = [
            {"uv_ratio": 0.1, "click_ad_ratio": 0.002, "url": "http://m.iqiyi.com/v_19rr6yfioc.html", "old_cookie": "", "video_info": {"cid": "12", "aid": "207638801", "tvid": "694669600", "sid": "0", "duration": "392", "shortTitle": "\\u4E00\\u665A\\u51E0\\u6B21\\u611F\\u89C9\\u8EAB\\u4F53\\u88AB\\u638F\\u7A7A", "vid": "f9436e2cd6b2e4ec26172697da486c04", "qipuId": "694669600"}, "ip": "", "process_number": 1, "videoid": "v_19rr6yfioc"},
            {"uv_ratio": 0.1, "click_ad_ratio": 0.002, "url": "http://m.iqiyi.com/v_19rr7epzlo.html", "old_cookie": "", "video_info": {"cid": "12", "aid": "684285200", "tvid": "684285200", "sid": "207270801", "duration": "207", "shortTitle": "diy\\u51E0\\u6B3E\\u597D\\u559D\\u53C8\\u597D\\u770B\\u7684\\u6C14\\u6CE1\\u6C34", "vid": "6184e10e9d68200d91a6ace9a172c0d5", "qipuId": "684285200"}, "ip": "", "process_number": 1, "videoid": "v_19rr7epzlo"},
            {"uv_ratio": 0.1, "click_ad_ratio": 0.002, "url": "http://m.iqiyi.com/v_19rr7eq0gw.html", "old_cookie": "", "video_info": {"cid": "12", "aid": "684284300", "tvid": "684284300", "sid": "207270801", "duration": "189", "shortTitle": "\\u6700\\u7F8E\\u4E0D\\u8FC7\\u679C\\u852C\\u6C99\\u62C9\\u9178\\u5976\\u9171\\u7684\\u9082\\u9005", "vid": "80197b3ecf0440f3a539936d877ace10", "qipuId": "684284300"}, "ip": "", "process_number": 1, "videoid": "v_19rr7eq0gw"},
            {"uv_ratio": 0.1, "click_ad_ratio": 0.002, "url": "http://m.iqiyi.com/v_19rr7eq0ck.html", "old_cookie": "", "video_info": {"cid": "12", "aid": "684284200", "tvid": "684284200", "sid": "207270801", "duration": "220", "shortTitle": "\\u708E\\u708E\\u590F\\u65E5\\uFF0C\\u4F60\\u9700\\u8981\\u4E00\\u5757\\u513F\\u897F\\u74DC", "vid": "87b0217fb115a2391d7ea542f049dd79", "qipuId": "684284200"}, "ip": "", "process_number": 1, "videoid": "v_19rr7eq0ck"}
        ]
        if os.path.isfile('storage_cookies.json'):
            print('读取cookie存档')
            logger.info('read cookie record')
            with open('storage_cookies.json', 'r', encoding='utf-8') as f:
                cookies = json.load(f)
        else:
            cookies = {}

        logger.info('start work')
        pool = multiprocessing.Pool()
        # process_number = 6
        # video_cookies = cookies.keys()
        tasks = []
        for task in iqiyi_new_4:
            print('每个链接的请求个数： %s' % task['process_number'])
            for num in range(task['process_number']):
                task['ip'] = ip
                if task['videoid'] in cookies.keys():
                    task['old_cookie'] = cookies[task['videoid']]
                else:
                    task['old_cookie'] = []
                tasks.append(task)
        # request_urls(task_parameters)
        # task = iqiyi_new_4[0]
        # task['ip'] = ip
        # request_urls(task)
        for return_info in pool.map(request_urls, tasks):
        # for task in tasks:
            # return_info = request_urls(task)
            logger.info('return info')
            logger.info(return_info)
            video_name, cookie_status, return_cookie = return_info
            if not cookie_status:
                # cookie_status在函数内部和使用旧cookie状态相同
                if video_name in cookies.keys():
                    cookies[video_name].append(return_cookie)
                    if len(cookies[video_name]) > 100:
                        cookies[video_name].pop(0)
                else:
                    cookies[video_name] = []
                    cookies[video_name].append(return_cookie)
            logger.info('storage cookies')
            with open('storage_cookies.json', 'w', encoding='utf-8') as f:
                f.write(json.dumps(cookies))
        # pool = multiprocessing.Pool()
        # cpus = multiprocessing.cpu_count()
        # for i in range(process_number):
        #     p = multiprocessing.Process(target=request_urls)
        #     logger.info('process %s start' % i)
        #     p.start()
        #     record.append(p)
        #     # time.sleep(3)
        # for p in record:
        #     p.join()

        logger.info('finish')
        # logger.debug('sleep')
        # time.sleep(random.choice(list(range(10, 30))))
        print('断开宽带')
        try:
            subprocess.Popen(["rasdial", "宽带连接", '/disconnect']).communicate()
        except BaseException as e:
            print(e)
        # time.sleep(5)
